import re
import requests
from bs4 import BeautifulSoup
import urllib.request
from urllib.request import urlopen
import csv
import sys
import os
import zipfile
import logging
import time
import datetime
import operator
from functools import reduce
from My_functions import edgar_scrapping_data
import pandas as pd
import numpy as np
from itertools import chain
from operator import add
from dateutil.relativedelta import *


c = '0001326801' #'0000051143'
cik = c.lstrip('0')
accession = '0001193125-12-325997' #0001193125-12-325997 0000051143-13-000007
acc = re.sub(r'[-]',r'', accession)
url = 'http://www.sec.gov/Archives/edgar/data/'+cik+'/'+acc+'/'+accession+'/-index.htm'

html = urlopen(url=url)
soup = BeautifulSoup(html, "lxml")
all_tables = soup.find('table', class_='tableFile')
tr = all_tables.find_all('tr')

for row in tr: #Get the first one.
    x = row.findNext("a").attrs['href']
    break


next_url = "http://www.sec.gov" + x

def ident_number_pattern(x):
    '''

    :param x: list of values
    :return: the unique values
    '''
    finlist = []
    for i in x:
        try:
            isinstance(float(i.replace(",", "")), float)
            finlist.append(i)
        except:
            pass
    # numeric_const_pattern = "[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?"
    # rx = re.compile(numeric_const_pattern, re.VERBOSE)
    return finlist

def date_find(x):

    finlist = []

    for i in x:
        current_month = (re.findall(r'(?:January|February|March|April|May|June|July|August|September|October|November|December)\s\d{2}', i))
        previous_month = (re.findall(r'One|Two|Three|Four|Five|Six|Seven|Eight|Nine|Ten|Eleven|Twelve', i))

        finlist.append(reduce(operator.concat, [previous_month, current_month]))
    return finlist


def months_dict(word_numb):
    '''
    :param word_numb: list of numbers (word)
    :return: return the corresponding value
    '''
    month_dict = {'One': '1', 'Two': '2', 'Three': '3', 'Four': '4', 'Five': '5', 'Six': '6', 'Seven': '7',
                  'Eight': '8', 'Nine': '9', 'Ten': '10', 'Eleven': '11', 'Twelve': '12'}

    return [int(month_dict[x]) for x in word_numb]

def reg_exprr_date(str_date):
    '''

    :param str_date: string value as '2012September 30'
    :return: return the corresponding pandas date format
    '''
    import pandas as pd
    month_dict = {"January": "01", "February": "02", "March": "03", "April": "04", "May": "05", "June": "06",
                  "July": "07", "August": "08", "September": "09", "October": "10", "November": "11", "December": "12"}

    year_exp = re.findall(r"[0-9]{4}", str_date)[0]
    day_exp = (re.findall(r"\s[0-9]{2}", str_date)[0]).replace(" ", "")
    month_exp = month_dict[re.findall(r'(?:January|February|March|April|May|June|July|August|September|October|November|December)', str_date)[0]]

    return pd.to_datetime(pd.datetime(year=int(year_exp), month=int(month_exp), day=int(day_exp)))

def get_soup(url):

    try:
        r = requests.get(url=url)
        return BeautifulSoup(r.text, "lxml") if r.status_code == 200 else None
    except:
        return None

soup = get_soup(next_url)
# print(soup)

##################### Pruebas
def checktag(param):
    setflag = "false"
    datatabletags = ["background", "bgcolor", "background-color"]
    for x in datatabletags:
        if x in param:
            setflag = "true"
    return setflag

def treat_neg_values(value):
    '''

    :param value: encode edgar value
    :return: if the value conteins ( change the value to negative
    '''
    if not "(" in value:
        pass
    else:

        aux = ((value).replace('(', '')).replace(')', '')
        try:
            isinstance(int(aux), int)
            value = '-' + aux
        except:
            pass
    return value

def printtable(table):
    logging.debug('In a function : printtable')
    printtable = []
    printtrs = table.find_all('tr')
    for tr in printtrs:
        data=[]
        pdata=[]
        printtds=tr.find_all('td')
        for elem in printtds:
            x=elem.text;
            # x=re.sub(r"['()]","",str(x))
            x=re.sub(r"[$]"," ",str(x))
            if(len(x)>1):
                x=re.sub(r"[—]","",str(x))
                pdata.append(x)
        data=([elem.encode('utf-8') for elem in pdata])
        printtable.append([treat_neg_values(elem.decode('utf-8').strip()) for elem in data])
    return printtable

def checkheadertag(param):
    logging.debug('In a function : checkheadertag')
    setflag="false"
    datatabletags=["center","bold"]
    for x in datatabletags:
        if x in param:
            setflag="true"
    return setflag

def foldername(page):
    title = page.find('filename').contents[0]
    if ".htm" in title:
        foldername = title.split(".htm")
        logging.debug('In the function : foldername{}'.format(foldername[0]))
        return foldername[0]

def assure_path_exists(path):
    logging.debug('In a function : assure_path_exists')
    if not os.path.exists(path):
        os.makedirs(path)


def table_to_csv(list_table):
    '''

    :param list_table: list of lists containing all data of table htm edgar
    :return: Returns a dataframe to export to csv
    '''
    date1 = 0
    date2 = 0
    finldict = []

    for l in range(0, len(list_table)):
        # l = 6
        ##### Find all individual values!
        all_values = ident_number_pattern(list_table[l])
        if len(all_values) == 0:

            if len(list(filter(None, date_find(list_table[l])))) > 0:
                date1 = list(filter(None, date_find(list_table[l])))

            elif len(''.join(list_table[l])) != 0:  # It because the items is a description
                # item_aux = [m if m != "" else np.nan for m in list_table[l][:len(date2)]]
                item_aux = np.full(len(date2), np.nan).tolist()
                item_aux.insert(0, ''.join(list_table[l]))
                finldict.append(item_aux)
            else:
                pass
        else:

            if (set(all_values)).intersection(set([str(o) for o in np.arange(2000, 2020, 1).tolist()])) == set(
                    all_values):
                date2 = all_values

            else:

                value_pos, val_min = [list_table[l].index(x) for x in all_values], min([list_table[l].index(x) for x in all_values])
                item_descr = ''.join(list_table[l][:val_min])
                values_list = [float((list_table[l][p]).replace(",", "")) for p in value_pos]
                values_list.insert(0, item_descr)
                finldict.append(values_list)

    modif_date1, ind = np.unique(re.findall(r'(?:January|February|March|April|May|June|July|August|September|October|November|December)\s\d{2}', ','.join(list(chain.from_iterable(date1)))), return_index=True)
    modif_date1 = list(modif_date1[np.argsort(ind)]) * len(date2) if len(modif_date1[np.argsort(ind)]) < 2 else modif_date1[np.argsort(ind)]
    con_dates = (list(map(add, date2, modif_date1)))

    table_date = [reg_exprr_date(x) for x in con_dates]
    aux = np.asarray(months_dict(re.findall(r'One|Two|Three|Four|Five|Six|Seven|Eight|Nine|Ten|Eleven|Twelve', ','.join(list(chain.from_iterable(date1))))))
    months_to_subtr = (np.repeat(aux, np.tile(int(len(table_date) / len(aux)), len(aux)))).tolist() if len(aux) > 0 else np.repeat(0, len(table_date))
    from_period = [table_date[x] - relativedelta(months=months_to_subtr[x]) for x in range(0, len(table_date))]
    def_dates = (list(map(add, [p.strftime('%m/%d/%Y') + '-' for p in from_period], [q.strftime('%m/%d/%Y') for q in table_date])))
    def_dates.insert(0, 'Description')
    fin_table = pd.DataFrame(finldict, columns=def_dates)

    return fin_table


# def find_all_datatables(page, all_divtables):


all_divtables = soup.find_all('table')

# logging.debug('In a function : find_all_datatables')
count = 0
hrcount = 0
allheaders = []

for table in all_divtables:
    # table = all_divtables[4]
    bluetables = []
    trs = table.find_all('tr')
    for tr in trs: ############# Through al rows of the table
        # tr = trs[2]
        global flagtr
        if checktag(str(tr.get('style'))) == "true" or checktag(str(tr)) == "true":
            logging.debug('Checking data tables at Row Level')
            bluetables = printtable(tr.find_parent('table'))
            break
        else:
            tds = tr.find_all('td')
            for td in tds:
                # td = tds[0] ########## checking data at column level
                if checktag(str(td.get('style'))) == "true" or checktag(str(td)) == "true":
                    logging.debug('Checking data tables at Column Level')
                    bluetables = printtable(td.find_parent('table'))
                    break
        if not len(bluetables) == 0:
            break
    if not len(bluetables) == 0:
        logging.debug('Total Number of data tables to be created {}'.format(len(bluetables)))
        count += 1
        ptag = table.find_previous('p')
        while ptag is not None and checkheadertag(ptag.get('style')) == "false": #and len(ptag.text) <= 1
            ptag = ptag.find_previous('p')
            if checkheadertag(ptag.get('style')) == "true" and len(ptag.text) >= 2:
                global name
                name = re.sub(r"[^A-Za-z0-9]+", "", ptag.text)
                if name in allheaders:
                    hrcount += 1
                    hrname = name+"_"+str(hrcount)
                    allheaders.append(hrname)
                else:
                    hrname=name
                    allheaders.append(hrname)
                    break

        folder_name = foldername(soup)
        logging.debug('folder created with folder Name{}'.format(folder_name))
        path = str(os.getcwd()) + "/" + folder_name
        logging.debug('Path for csv creation {}'.format(path))
        assure_path_exists(path)
        if len(allheaders) == 0:
            filename = folder_name+"-"+str(count)
        else:
            filename = allheaders.pop()
        csvname = filename+".csv"
        logging.debug('file creation Name{}'.format(csvname))
        csvpath = path + "/" + csvname
        logging.debug('CSV Path for the file creation {}'.format(csvpath))
        ((table_to_csv(bluetables)).set_index('Description')).to_csv(csvpath, sep=';', index=True)
        prueba = (table_to_csv(bluetables))
        # # with open(csvpath, 'w', encoding='utf-8-sig', newline='') as f:
        # #     writer = csv.writer(f)
        # #     writer.writerows(table_to_csv(bluetables))
        # zip_dir(path)


list_table = bluetables
date1 = 0
date2 = 0
finldict = []

for l in range(0, len(list_table)):
    # l = 2
    ##### Find all individual values!
    all_values = ident_number_pattern(list_table[l])
    if len(all_values) == 0:

        if len(list(filter(None, date_find(list_table[l])))) > 0:
            date1 = list(filter(None, date_find(list_table[l])))

        elif len(''.join(list_table[l])) != 0:  # It because the items is a description
            # item_aux = [m if m != "" else np.nan for m in list_table[l][:len(date2)]]
            item_aux = np.full(len(date2), np.nan).tolist()
            item_aux.insert(0, ''.join(list_table[l]))
            finldict.append(item_aux)
        else:
            pass
    else:

        if (set(all_values)).intersection(set([str(o) for o in np.arange(2000, 2020, 1).tolist()])) == set(
                all_values):
            date2 = all_values

        else:

            value_pos, val_min = [list_table[l].index(x) for x in all_values], min([list_table[l].index(x) for x in all_values])
            item_descr = ''.join(list_table[l][:val_min])
            values_list = [float((list_table[l][p]).replace(",", "")) for p in value_pos]
            values_list.insert(0, item_descr)
            finldict.append(values_list)

modif_date1, ind = np.unique(re.findall(r'(?:January|February|March|April|May|June|July|August|September|October|November|December)\s\d{2}', ','.join(list(chain.from_iterable(date1)))), return_index=True)
modif_date1 = list(modif_date1[np.argsort(ind)]) * len(date2) if len(modif_date1[np.argsort(ind)]) < 2 else modif_date1[np.argsort(ind)]
con_dates = (list(map(add, date2, modif_date1)))

table_date = [reg_exprr_date(x) for x in con_dates]
aux = np.asarray(months_dict(re.findall(r'One|Two|Three|Four|Five|Six|Seven|Eight|Nine|Ten|Eleven|Twelve', ','.join(list(chain.from_iterable(date1))))))
months_to_subtr = (np.repeat(aux, np.tile(int(len(table_date) / len(aux)), len(aux)))).tolist() if len(aux) > 0 else np.repeat(0, len(table_date))
from_period = [table_date[x] - relativedelta(months=months_to_subtr[x]) for x in range(0, len(table_date))]
def_dates = (list(map(add, [p.strftime('%m/%d/%Y') + '-' for p in from_period], [q.strftime('%m/%d/%Y') for q in table_date])))
def_dates.insert(0, 'Description')
fin_table = pd.DataFrame(finldict, columns=def_dates)

