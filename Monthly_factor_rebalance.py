import pandas as pd
import numpy as np
import sqlite3

######### OPTIMIZATION ############################
import scipy.optimize as optim
import cvxopt as opt
from cvxopt import solvers
from My_functions import Clustering as cluster_corr
from My_functions import Garch_function as garch
import scipy.stats as stats
from arch import arch_model


# Connection to databases sql
conn_NASD = sqlite3.connect("C:\\Users\\pccom\\OneDrive\\HistoricalDataReuters\\SQLitte_DataBase\\NASDAQ_Sep.db")
conn_NYSE = sqlite3.connect("C:\\Users\\pccom\\OneDrive\\HistoricalDataReuters\\SQLitte_DataBase\\NYSE_Sep.db")
conn_sp500 = sqlite3.connect("C:\\Users\\pccom\\OneDrive\\HistoricalDataReuters\\SQLitte_DataBase\\SP500.db")


end_date_variables = pd.datetime(year=2018, month=9, day=1)
date_elimin_susp_assets = pd.datetime(year=end_date_variables.year, month=7, day=1)



####################### FUNCTIONS ##########################
def load_variable(var_name,  exchange, fechalim):
    '''

    :param var_name: Name of the variable to load. Check the SQL table
    :param exchange: the exchange connection
    :param fechalim: la fecha final a tener en consideración tiene que ser datetime o 0 si se quiere coger el periodo entero.

    :return: Return a DataFrame with all assets of the specified variable
    '''


    list_table = []
    for j in range(0, 5):

        if j == 0:
            aux = pd.read_sql_query(''' SELECT * from "%s" '''% ("{}".format(var_name) + ".xlsx.xlsx_" + str(j)), exchange)
        else:
            aux = pd.read_sql_query(''' SELECT * from "%s" '''% ("{}".format(var_name) + ".xlsx.xlsx_" + str(j)), exchange)
            aux.drop(aux.columns[[0]], axis=1, inplace=True)

        list_table.append(aux)

    Final_Table = pd.concat(list_table, axis=1, sort=True)
    Final_Table = Final_Table.set_index("Date")
    Final_Table.index = pd.to_datetime(Final_Table.index)
    if fechalim != 0:
        Final_Table = Final_Table.loc[Final_Table.index <= fechalim]

        return Final_Table
    else:
        return Final_Table

def month_momentum(w):
    def aux(x):
        return (np.prod((x[:-1]+1))-1)*w
    return aux

def momentum_pos_neg(w):
    def aux1(x):

        return ((np.sum(x<0) / np.sum(x!=0)) - (np.sum(x>0)/np.sum(x!=0)))*w

    return aux1

def daily_to_monthly(data):
    '''
    :param data: DataFrame of daily data to convert with index of Datatime
    :return: Return DataFrame only with the last date of the month
    '''

    from pandas.tseries.offsets import BMonthEnd
    offset = BMonthEnd()
    data['BMonthEnd'] = [offset.rollforward(data.index[x]) for x in range(0, len(data.index))]
    MV_NYSE_monthly = data.loc[pd.to_datetime(data.index) == data['BMonthEnd']] #Me paso los datos diarios a mensuales (fin del mes)

    return MV_NYSE_monthly


def MV_breakpoint(date, market_base):
    '''

    :param date: date in wich to calculate the break-point
    :param market_base: the market date to use as base

    :return: Return the value of the break-point
    '''

    return (np.median(market_base.loc[date].dropna().values))

def find_interes_list(list1, list2, list3):
    '''
    This function is only for the ROE IA and Size factors
    :param list1: assets of some ROE/IA/SIZE
    :param list2: assets of some ROE/IA/SIZE
    :param list3: assets of some ROE/IA/SIZE

    :return: Returns the interesection of the three lists
    '''

    return set.intersection(*map(set, [list1, list2, list3]))

#### Función para poder calcular la media de los últimos 4 trimestres de datos en rolling window
def f(w):
    def g(x):
        return (w*x).sum()
    return g


def calc_rend_IAROE(table_precios, table_mv, date1, date2, assets_compute):
    '''

    :param table_precios: Table where all prices are in
    :param table_mv: Table where all mv are in
    :param date1: date beging
    :param date2: date end
    :param assets_compute: Assets to compute

    :return: Returns an vector of returns
    '''

    weights = table_mv.loc[table_mv.index == date1][list(assets_compute)].T.fillna(0)
    vector_of_ret = (((table_precios[list(assets_compute)].loc[((table_precios.index >= date1) & (table_precios.index <= date2))])).pct_change(1).fillna(0).values).\
                    dot(weights.values / np.sum(weights.values))

    return vector_of_ret


def MonteCarlo(rend, nSims):
    '''

    :param rend: Historical daily returns in Data Frame
    :param nSims: number of simulation of the Monte Carlo to employed. Take into account that they have to be at leas 10

    :return: matriz of all simulations
    '''

    import numpy.random as ra
    import matplotlib.pyplot as plt
    import scipy.linalg as la
    from scipy import stats

    mat_corr = rend.corr().values
    n, _ = mat_corr.shape
    mat_cholesky = la.cholesky(mat_corr)  # The output of the function is superior triangualr matrix but we want the inferior
    mat_cholesky_inf = la.cholesky(mat_corr).T  # We have the inferior Cholesky matrix
    ys_random_norm = stats.norm.rvs(size=(n, nSims))
    # print("Medias: ", ys_random_norm.mean(axis=0))
    # print("Varianzas: ", ys_random_norm.var(axis=0))
    # print("Correlaciones: ", np.corrcoef(ys_random_norm))

    chol_aux = np.transpose(la.inv(la.cholesky(
        np.cov(ys_random_norm)).T))  # A 1-D or 2-D array containing multiple variables and observations. Each row of x
    # represents a variable, and each column a single observation of all those variables.

    ys_new = np.dot(ys_random_norm.T, chol_aux)  # variables aleatorios modificadas
    ys_correlated = np.dot(ys_new, mat_cholesky_inf.T)  # Cada columna una variable

    stand_dev = np.diag(rend.std(axis=0).values)  # This are the std of the origianl series and I will use it to modify the MC sim
    orig_mus = np.tile((np.mean(rend.values, axis=0, dtype=np.float64)), (ys_correlated.shape[0], 1))

    final_res = np.dot(ys_correlated, stand_dev) + orig_mus

    return final_res


def ResampelEF_func(returns, mus, numSimulation):
    '''
    :param returns: Historical daily returns for the portfolio (Dataframe)
    :param mus: Mean for which to find the optimal portfolio annualized (np.array)
    :param numSimulation: Number of the iterations of the resample

    :return: returns optimal portfolio weights and corresponding sigmas for a desired optimal portfolio return
    '''
    # Name of all assets
    Index_names = returns.columns.values
    weight_dict = {}
    dict_final = {}
    Ef = {}

    cov = np.matrix(np.cov(returns.T) * 252)  # Matrix covariance original
    _, N = returns.shape

    orig_mus = np.mean(returns, axis=0) * 252  # Original mean of all variables

    # total_mus = np.vstack((orig_mus, random_sample))
    optimal_mus = mus.tolist()
    # constraint matrices for quadratic programming
    P = opt.matrix(cov)
    q = opt.matrix(np.zeros((N, 1)))
    A = opt.matrix(1.0, (1, N))
    b = opt.matrix(1.0)

    # Solve for the origianls mu
    pbar = np.matrix(orig_mus)
    G = opt.matrix(np.concatenate((-np.array(pbar), -np.identity(N)), 0))
    # hide optimization
    opt.solvers.options['show_progress'] = False

    # calculate portfolio weights, every weight vector is of size Nx1
    # find optimal weights with qp(P, q, G, h, A, b)

    optimal_weights = [
        solvers.qp(P, q, G, opt.matrix(np.concatenate((-np.ones((1, 1)) * mu, np.zeros((N, 1))), 0)), A, b)['x'] for mu
        in optimal_mus]

    optimal_sigmas = [np.sqrt(np.matrix(w).T * cov.T.dot(np.matrix(w)))[0, 0] for w in optimal_weights]

    for asset in range(0, len(Index_names)):

        for point in range(0, len(mus)):
            weight_dict.setdefault(Index_names[asset] + "_0", []).append(optimal_weights[point][asset])

    iteration = 1
    # Solve for different simulations
    while iteration != numSimulation:

        simret = MonteCarlo(returns, 10) * 252
        pbar = np.matrix(np.mean(simret, axis=0))
        G = opt.matrix(np.concatenate((-np.array(pbar), -np.identity(N)), 0))
        # hide optimization
        opt.solvers.options['show_progress'] = False

        # calculate portfolio weights, every weight vector is of size Nx1
        # find optimal weights with qp(P, q, G, h, A, b)
        try:
            optimal_weights = [
                solvers.qp(P, q, G, opt.matrix(np.concatenate((-np.ones((1, 1)) * mu, np.zeros((N, 1))), 0)), A, b)['x']
                for mu
                in optimal_mus]

            optimal_sigmas = [np.sqrt(np.matrix(w).T * cov.T.dot(np.matrix(w)))[0, 0] for w in optimal_weights]

            for asset in range(0, len(Index_names)):

                for point in range(0, len(mus)):
                    weight_dict.setdefault(Index_names[asset] + "_%s" % iteration, []).append(
                        optimal_weights[point][asset])

            iteration += 1
        except:
            print("%f" % iteration)

    assets_all = list(set([x.split('_')[:-1][0] for x in list(weight_dict.keys())]))
    for y in assets_all:
        dict_final[y] = np.mean(np.vstack([np.asarray(weight_dict[y + '_%s' % i]) for i in range(iteration)]), axis=0)

    EF_final_resutls = pd.DataFrame.from_dict(dict_final, orient='columns')

    for ef in range(len(mus)):
        media = np.dot(EF_final_resutls.loc[ef:ef].values, (np.mean(returns.values, axis=0, dtype=np.float64)))[0]
        std = np.sqrt(np.dot(np.dot(EF_final_resutls.loc[ef:ef].values, returns.cov().values),
                             EF_final_resutls.loc[:0].values.T))[0][0]
        Ef[ef] = [media, std]

    EF_final_resutls = (EF_final_resutls.join(pd.DataFrame.from_dict(Ef, orient='index'))).rename(
        columns={0: 'ExpReturn', 1: 'StandardDev'})
    EF_final_resutls["SharpeRatio"] = EF_final_resutls['ExpReturn'] / EF_final_resutls['StandardDev']

    opt_portf = EF_final_resutls.loc[EF_final_resutls['SharpeRatio'] == EF_final_resutls['SharpeRatio'].max()]

    return opt_portf, EF_final_resutls


def EF_factors_optimization(data_table, point_sep_EF, num_sim_MC, factor_to_use):
    '''

    :param data_table: DataFrame with all factors
    :param point_sep_EF: point to separate the EF in order to use the modified EF calculation
    :param num_sim_MC: MonteCarlo number of simulation
    :param factor_to_use: Name of the factor to use, should be contained in data_table

    :return: Return the optimal weights of a portfolio
    '''

    optimization_return = data_table[factor_to_use]
    mu = (np.mean(optimization_return.values, axis=0, dtype=np.float64))
    mat_cov = np.cov(optimization_return.values.T)
    xini = np.ndarray((len(mu)))  # Define the intial value iteration

    def portfolio_std(weight):
        '''
        :param weight: numpy vector of weights where each elements is correspondent assets
        :return: The standard deviation of the portfolio
        '''
        import numpy as np
        result = (np.sqrt(np.matmul(np.matmul(weight, mat_cov), weight)))
        return result

    def portfolio_return(mu, weight):
        '''
        :param mu: numpy vector of assets return
        :param weight: numpy vector of weights where each elements is correspondent assets

        :return: Expected returns of the portfolio multiplied by 1000000 and in oposite value
        '''
        import numpy as np

        if mu.shape[0] == weight.shape[0]:

            p_ret = np.dot(mu, weight)
        else:
            p_ret = np.dot(mu, weight.T)

        return -(p_ret) * 1000000

    # Se definen las restricciones
    bounds = [(0, 1) for x in range(0, len(mu))]  # Defino el rango del resultado que sea entre 0 y 1 inclusive

    def con(weight):
        return np.sum(weight) - 1  # si se quieren añadir mas restricciones se añade una lista de diccionarios

    # Resolución el problema
    solMLEopt = optim.minimize(portfolio_return, xini, args=(mu,), method='SLSQP', bounds=bounds,
                               constraints={'type': 'eq', 'fun': con})

    solMLEopt_minstd = optim.minimize(portfolio_std, xini, method='SLSQP', bounds=bounds,
                                      constraints={'type': 'eq', 'fun': con})

    # Maximum return result and its correspondent std
    max_expected_mean = - (portfolio_return(mu, solMLEopt.x)) * 252 / 1000000
    std_dev = np.sqrt(np.matmul(np.matmul(solMLEopt.x, mat_cov), solMLEopt.x)) * np.sqrt(252)

    # Minimum std result and its correspondent expected return
    min_std = portfolio_std(solMLEopt_minstd.x) * np.sqrt(252)
    port_ret = - portfolio_return(mu, solMLEopt_minstd.x) * 252 / 1000000

    increment = (max_expected_mean - port_ret) / (point_sep_EF - 1)

    EF_increment = (np.arange(port_ret, max_expected_mean + increment, increment))[:point_sep_EF]

    optimo, _ = ResampelEF_func(returns=optimization_return, mus=EF_increment, numSimulation=num_sim_MC)

    return optimo


############## Load the variable from sql needed to perform the strategy ###################


NETINCOM_NYSE = load_variable(var_name= "Total_net_income", exchange=conn_NYSE, fechalim=end_date_variables)
NETINCOM_NASDAQ = load_variable(var_name= "Total_net_profit", exchange=conn_NASD, fechalim=end_date_variables)

CASHOPER_NYSE = load_variable(var_name= "cash_flow_oper", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
CASHOPER_NASDAQ = load_variable(var_name= "CASHFLOWOPER", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

TASSETS_NYSE = load_variable(var_name= "TotalAssets", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
TASSETS_NASDAQ = load_variable(var_name= "TotalAssets", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

CASHEQUI_NYSE = load_variable(var_name= "cash_equivalent", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
CASHEQUI_NASDAQ = load_variable(var_name= "cash_equivalent", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

LTD_NYSE = load_variable(var_name= "LTDEBT", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
LTD_NASDAQ = load_variable(var_name= "LTDEBT", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

STDEBT_NYSE = load_variable(var_name= "STDEBT", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
STDEBT_NASDAQ = load_variable(var_name= "STDEBT", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

MININTER_NYSE = load_variable(var_name= "MINORITY_INTEREST", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
MININTER_NASDAQ = load_variable(var_name= "MINORITY_INTEREST", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

PREFSTOCK_NYSE = load_variable(var_name= "PREFERRED_STOCK", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
PREFSTOCK_NASDAQ = load_variable(var_name= "PREFERRED_STOCK", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

COMMEQUI_NYSE = load_variable(var_name= "CommEquity", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
COMMEQUI_NASDAQ = load_variable(var_name= "CommEquity", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

FiscalYear_NYSE = pd.read_sql_query(" SELECT * FROM {}".format("[Fiscal_dayEnd.xlsx.xlsx]"), con=conn_NYSE)
FiscalYear_NASDAQ = pd.read_sql_query(" SELECT * FROM {}".format("[FiscaldayEnd.xlsx.xlsx]"), con=conn_NASD)

RericDay_NYSE = load_variable(var_name= "ACCOUNT_RECEIV_DAYS", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
RericDay_NASDAQ = load_variable(var_name= "ACCOUNT_RECEIV_DAYS", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

Gross_margin_NYSE = load_variable(var_name= "GROSS_PROFIT_margin", exchange=conn_NYSE,fechalim=end_date_variables) #Mensuales
Gross_margin_NASDAQ = load_variable(var_name= "GROSS_PROFIT MARGIN", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

CurrAsset_NYSE = load_variable(var_name= "CURRENT_ASSETS", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
CurrAsset_NASDAQ = load_variable(var_name= "CURRENT_ASSETS", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

PPE_NYSE = load_variable(var_name= "PP_E", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
PPE_NASDAQ = load_variable(var_name= "PP_E", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

NET_sales_NYSE = load_variable(var_name= "NET_SALES", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
NET_sales_NASDAQ = load_variable(var_name= "NET_SALES", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

Amort_NYSE = load_variable(var_name= "AMORT", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
Amort_NASDAQ = load_variable(var_name= "AMORT", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

GrossProfit_NYSE = load_variable(var_name= "GROSS_PROFIT", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
GrossProfit_NASDAQ = load_variable(var_name= "GROSS_PROFIT", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

OperatingIncome_NYSE = load_variable(var_name= "OPERATING_INCOME", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
OperatingIncome_NASDAQ = load_variable(var_name= "OPERATING_INCOME", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

TotalDebt_NYSE = load_variable(var_name= "TOTAL_DEBT", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
TotalDebt_NASDAQ = load_variable(var_name= "TOTAL_DEBT", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

TotalLiab_NYSE = load_variable(var_name= "TOTAL_LIAB", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
TotalLiab_NASDAQ = load_variable(var_name= "TOTAL_LIAB", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

MV_NYSE = load_variable(var_name= "Market_Value", exchange=conn_NYSE, fechalim=end_date_variables)
MV_Nasdaq = load_variable(var_name= "MarketValue", exchange=conn_NASD, fechalim=end_date_variables)

Prices_NYSE = load_variable(var_name="Prices", exchange=conn_NYSE, fechalim=0)
Prices_Nasdaq = load_variable(var_name="Prices", exchange=conn_NASD,fechalim=0)

VIX = (pd.read_sql_query("SELECT * FROM VIX", con=conn_sp500)).set_index("Date")
VIX.index = pd.to_datetime(VIX.index)

NetWorkCap_NYSE = load_variable(var_name= "Net_work_capital", exchange=conn_NYSE, fechalim=end_date_variables)
NetWorkCap_Nasdaq = load_variable(var_name= "Net_working_cap", exchange=conn_NASD, fechalim=end_date_variables)

NETINCOMbeforeExtr_NYSE = load_variable(var_name= "net_income_before_extra", exchange=conn_NYSE, fechalim=end_date_variables) #Mensuales
NETINCOMbeforeExtr_NASDAQ = load_variable(var_name= "NET_INCOME_BEF_EXTR", exchange=conn_NASD, fechalim=end_date_variables) #Mensuales

InterExpanse_NYSE = load_variable(var_name= "INTEREST_EXPANSE", exchange=conn_NYSE, fechalim=end_date_variables)
InterExpanse_Nasdaq = load_variable(var_name= "INTEREST_EXPANSE", exchange=conn_NASD, fechalim=end_date_variables)

Inventory_NYSE = load_variable(var_name="Inventory", exchange=conn_NYSE, fechalim=end_date_variables)
Inventory_Nasdaq = load_variable(var_name="Inventory", exchange=conn_NASD, fechalim=end_date_variables)

ROE_NYSE = load_variable(var_name="ROE", exchange=conn_NYSE, fechalim=end_date_variables)
ROE_Nasdaq = load_variable(var_name="ROE", exchange=conn_NASD, fechalim=end_date_variables)

MTBV_NYSE = load_variable(var_name="MTBV", exchange=conn_NYSE, fechalim=end_date_variables)
MTBV_Nasdaq = load_variable(var_name="MTBV", exchange=conn_NASD, fechalim=end_date_variables)

ID_COMPANY_NYSE = pd.read_sql_query(''' select * from "SIC_CODE.xlsx.xlsx" ''', conn_NYSE)
ID_COMPANY_Nasdaq = pd.read_sql_query(''' select * from "SIC_Code.xlsx.xlsx" ''', conn_NASD)


############### Needed to identify the suspicious assets ################################
MV_SP500 = (pd.read_sql_query("SELECT * FROM Market_value_daily", con=conn_sp500)).set_index("Date")
Prices_SP500 = (pd.read_sql_query("SELECT * FROM Prices_daily", con=conn_sp500)).set_index("Date")

Prices_SP500.index = pd.to_datetime(Prices_SP500.index)
Prices_NYSE.index = pd.to_datetime(Prices_NYSE.index)
Prices_Nasdaq.index = pd.to_datetime(Prices_Nasdaq.index)
MV_SP500.index = pd.to_datetime(MV_SP500.index)

### NYSE ####
TASSETS_NYSE = TASSETS_NYSE.apply(pd.to_numeric, errors='coerce')
STDEBT_NYSE = STDEBT_NYSE.apply(pd.to_numeric, errors='coerce')
LTD_NYSE = LTD_NYSE.apply(pd.to_numeric, errors='coerce')
PREFSTOCK_NYSE = PREFSTOCK_NYSE.apply(pd.to_numeric, errors='coerce')
MININTER_NYSE = PREFSTOCK_NYSE.apply(pd.to_numeric, errors='coerce')
COMMEQUI_NYSE = COMMEQUI_NYSE.apply(pd.to_numeric, errors='coerce')
CASHEQUI_NYSE = CASHEQUI_NYSE.apply(pd.to_numeric, errors='coerce')
NETINCOM_NYSE = NETINCOM_NYSE.apply(pd.to_numeric, errors='coerce')
CASHOPER_NYSE = CASHOPER_NYSE.apply(pd.to_numeric, errors='coerce')
RericDay_NYSE = RericDay_NYSE.apply(pd.to_numeric, errors='coerce')
Gross_margin_NYSE = Gross_margin_NYSE.apply(pd.to_numeric, errors='coerce')
CurrAsset_NYSE = CurrAsset_NYSE.apply(pd.to_numeric, errors='coerce')
PPE_NYSE = PPE_NYSE.apply(pd.to_numeric, errors='coerce')
NET_sales_NYSE = NET_sales_NYSE.apply(pd.to_numeric, errors='coerce')
Amort_NYSE = Amort_NYSE.apply(pd.to_numeric, errors='coerce')
GrossProfit_NYSE = GrossProfit_NYSE.apply(pd.to_numeric, errors='coerce')
OperatingIncome_NYSE = OperatingIncome_NYSE.apply(pd.to_numeric, errors='coerce')
TotalDebt_NYSE= TotalDebt_NYSE.apply(pd.to_numeric, errors='coerce')
TotalLiab_NYSE = TotalLiab_NYSE.apply(pd.to_numeric, errors='coerce')
MV_NYSE = MV_NYSE.apply(pd.to_numeric, errors='coerce')
NetWorkCap_NYSE = NetWorkCap_NYSE.apply(pd.to_numeric, errors='coerce')
NETINCOMbeforeExtr_NYSE = NETINCOMbeforeExtr_NYSE.apply(pd.to_numeric, errors='coerce')
InterExpanse_NYSE = InterExpanse_NYSE.apply(pd.to_numeric, errors='coerce')
Inventory_NYSE = Inventory_NYSE.apply(pd.to_numeric, errors='coerce')
ROE_NYSE = ROE_NYSE.apply(pd.to_numeric, errors='coerce')
MTBV_NYSE = MTBV_NYSE.apply(pd.to_numeric, errors='coerce')
Prices_NYSE = Prices_NYSE.apply(pd.to_numeric, errors='coerce')


Prices_SP500 = Prices_SP500.apply(pd.to_numeric, errors='coerce')
MV_SP500 = MV_SP500.apply(pd.to_numeric, errors='coerce')

#### NASDAQ ####
TASSETS_NASDAQ = TASSETS_NASDAQ.apply(pd.to_numeric, errors='coerce')
STDEBT_NASDAQ = STDEBT_NASDAQ.apply(pd.to_numeric, errors='coerce')
LTD_NASDAQ = LTD_NASDAQ.apply(pd.to_numeric, errors='coerce')
PREFSTOCK_NASDAQ = PREFSTOCK_NASDAQ.apply(pd.to_numeric, errors='coerce')
MININTER_NASDAQ = PREFSTOCK_NASDAQ.apply(pd.to_numeric, errors='coerce')
COMMEQUI_NASDAQ = COMMEQUI_NASDAQ.apply(pd.to_numeric, errors='coerce')
CASHEQUI_NASDAQ = CASHEQUI_NASDAQ.apply(pd.to_numeric, errors='coerce')
NETINCOM_NASDAQ = NETINCOM_NASDAQ.apply(pd.to_numeric, errors='coerce')
CASHOPER_NASDAQ = CASHOPER_NASDAQ.apply(pd.to_numeric, errors='coerce')
RericDay_NASDAQ = RericDay_NASDAQ.apply(pd.to_numeric, errors='coerce')
Gross_margin_NASDAQ = Gross_margin_NASDAQ.apply(pd.to_numeric, errors='coerce')
CurrAsset_NASDAQ = CurrAsset_NASDAQ.apply(pd.to_numeric, errors='coerce')
PPE_NASDAQ = PPE_NASDAQ.apply(pd.to_numeric, errors='coerce')
NET_sales_NASDAQ = NET_sales_NASDAQ.apply(pd.to_numeric, errors='coerce')
Amort_NASDAQ = Amort_NASDAQ.apply(pd.to_numeric, errors='coerce')
GrossProfit_NASDAQ = GrossProfit_NASDAQ.apply(pd.to_numeric, errors='coerce')
OperatingIncome_NASDAQ = OperatingIncome_NASDAQ.apply(pd.to_numeric, errors='coerce')
TotalDebt_NASDAQ = TotalDebt_NASDAQ.apply(pd.to_numeric, errors='coerce')
TotalLiab_NASDAQ = TotalLiab_NASDAQ.apply(pd.to_numeric, errors='coerce')
MV_Nasdaq = MV_Nasdaq.apply(pd.to_numeric, errors='coerce')
NetWorkCap_Nasdaq = NetWorkCap_Nasdaq.apply(pd.to_numeric, errors='coerce')
NETINCOMbeforeExtr_NASDAQ = NETINCOMbeforeExtr_NASDAQ.apply(pd.to_numeric, errors='coerce')
InterExpanse_Nasdaq = InterExpanse_Nasdaq.apply(pd.to_numeric, errors='coerce')
Inventory_Nasdaq = Inventory_Nasdaq.apply(pd.to_numeric, errors='coerce')
ROE_Nasdaq = ROE_Nasdaq.apply(pd.to_numeric, errors='coerce')
MTBV_Nasdaq = MTBV_Nasdaq.apply(pd.to_numeric, errors='coerce')
Prices_Nasdaq = Prices_Nasdaq.apply(pd.to_numeric, errors='coerce')


## NYSE
PPE_NYSE_June = PPE_NYSE.loc[pd.to_datetime(PPE_NYSE.index).month == 7]
delta_PPE_NYSE_June = PPE_NYSE_June -PPE_NYSE_June.shift(1)

Invent_NYSE_June = Inventory_NYSE.loc[pd.to_datetime(Inventory_NYSE.index).month == 7]
delta_INV_NYSE_June = Invent_NYSE_June -Invent_NYSE_June.shift(1)

TA_NYSE_June = TASSETS_NYSE.loc[pd.to_datetime(TASSETS_NYSE.index).month == 7]

IA_NYSE = (delta_PPE_NYSE_June.add(delta_INV_NYSE_June, fill_value=0))/TA_NYSE_June.shift(1) # Hago la suma pero si uno de los elementos contiene nan y el otro tiene un valor, coge el valor real.

#     ### NASDAQ
PPE_Nasdaq_June = PPE_NASDAQ.loc[pd.to_datetime(PPE_NASDAQ.index).month == 7]
delta_PPE_Nasdaq_June = PPE_Nasdaq_June -PPE_Nasdaq_June.shift(1)

Invent_Nasdaq_June = Inventory_Nasdaq.loc[pd.to_datetime(Inventory_Nasdaq.index).month == 7]
delta_INV_Nasdaq_June = Invent_Nasdaq_June - Invent_Nasdaq_June.shift(1)

TA_Nasdaq_June = TASSETS_NASDAQ.loc[pd.to_datetime(TASSETS_NASDAQ.index).month == 7]
TA_Nasdaq_June = TA_Nasdaq_June.replace(0, np.nan)

IA_Nasdaq = (delta_PPE_Nasdaq_June.add(delta_INV_Nasdaq_June, fill_value=0))/TA_Nasdaq_June.shift(1) # Hago la suma pero si uno de los elementos contiene nan y el otro tiene un valor, coge el valor real.


All_prices = pd.concat([Prices_Nasdaq, Prices_NYSE], axis=1, sort=True)
All_prices = All_prices.apply(pd.to_numeric, errors='coerce')

# MOMENTUM INPUTS CONSTRUCTION
All_month_momentum_returns = (All_prices.resample('BMS').asfreq().pct_change(1)).rolling(window=12).apply(month_momentum(1), raw=True)
All_porc_negpos = All_prices.pct_change(1).rolling(window=252).apply(momentum_pos_neg(1), raw=True)
All_porc_negpos = All_porc_negpos.resample('BMS').asfreq()

OP_NYSE = (OperatingIncome_NYSE.subtract(InterExpanse_NYSE.fillna(0), axis=1)).divide(COMMEQUI_NYSE, axis=1)
OP_NASDAQ = (OperatingIncome_NASDAQ.subtract(InterExpanse_Nasdaq.fillna(0), axis=1)).divide(COMMEQUI_NASDAQ, axis=1)

Inv_NYSE = TASSETS_NYSE.loc[TASSETS_NYSE.index.month == 7].pct_change(1)  # .resample("6MS", label='right').asfreq()
Inv_NASDAQ = TASSETS_NASDAQ.loc[TASSETS_NASDAQ.index.month == 7].pct_change(1)

Financial_company = pd.concat([ID_COMPANY_NYSE, ID_COMPANY_Nasdaq], axis=0, sort=True)
Financial_company = list(Financial_company.loc[((Financial_company["SIC CODE 1"] >= 6000 ) & (Financial_company["SIC CODE 1"] <= 6799))]['Company_Mnemonic'].values)

All_market_values = MV_NYSE.join(MV_Nasdaq, how='left')
value_weights_monthly = All_market_values.divide(All_market_values.sum(axis=1).values,axis=0)

VIX_aux = pd.concat([VIX] * 11, axis=1, ignore_index=True, sort=True)
VIX_aux.columns = ['INV', 'ROE', 'RM', 'SMBbm', 'SMB', 'HML', 'RMW', 'CMA', 'MOMENTUM', 'MOMBUYONLY', 'LowVolatility'] #'LowVolatility'

static_std = pd.Series(np.array([14.0, 30.0, 16.0, 7.67, 80.0, 10.21, 34.0, 15.30, 6.83, 43.0, 10.0]), index=["CMA", "RMW", "HML", "SMBbm", "MOMBUYONLY", "INV", "ROE", "RM", "SMB", "MOMENTUM", "LowVolatility"]) #"LowVolatility"


##### Parameters #####
static = True
VIX_used = True
errors_date = [] ### To follow how many times the EF algorithm falls
Garch_used = False

id_dates_1 = list(ROE_Nasdaq.index[(pd.to_datetime(ROE_Nasdaq.index)).searchsorted(pd.datetime(year=2000, month=7, day=1)):].sort_values(ascending=True)) # The initial date is 01/07/2000 because of the database available

Total_portf_returns = {}
Total_summary_investment = {}

################## MAIN CALCULATIONS #########

for dat in range(0, len(id_dates_1)):

    # dat = 1
    #Rank del SIZE

    NYSE_aux_1 = (MV_NYSE.loc[(MV_NYSE.index.year == id_dates_1[dat].year) &
                                         (MV_NYSE.index.month == id_dates_1[dat].month)]).T.dropna()
    Nasdaq_aux_1 = (MV_Nasdaq.loc[(MV_Nasdaq.index.year == id_dates_1[dat].year) &
                                (MV_Nasdaq.index.month == id_dates_1[dat].month)].T).dropna()

    NYSE_aux_1 = NYSE_aux_1.loc[~NYSE_aux_1.index.isin(Financial_company)] # Elimino aquellas empresa que en ese momento se consideran financieras
    Nasdaq_aux_1 = Nasdaq_aux_1.loc[~Nasdaq_aux_1.index.isin(Financial_company)]  # Elimino aquellas empresa que en ese momento se consideran financieras

    perc_40_liquidity_1 = np.percentile((NYSE_aux_1.values), 40)

    per_size_low_nyse = np.percentile(NYSE_aux_1.values[NYSE_aux_1.values >= perc_40_liquidity_1], 10)
    per_size_high_nyse = np.percentile(NYSE_aux_1.values[NYSE_aux_1.values >= perc_40_liquidity_1], 90)

    aux_size_nyseNasdaq = pd.concat([Nasdaq_aux_1.rename(columns={id_dates_1[dat]: "SIZE"}), NYSE_aux_1.rename(columns={id_dates_1[dat]: "SIZE"})], axis=0, sort=True)

    iliquid_assets = aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["SIZE"] < perc_40_liquidity_1].index

    aux_size_nyseNasdaq = aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["SIZE"] >= perc_40_liquidity_1]


    #Rank del ROE
    aux_roe_nyse = (ROE_NYSE.loc[pd.to_datetime(ROE_NYSE.index) == pd.to_datetime(id_dates_1[dat])]).T.dropna()
    aux_roe_nasdaq = (ROE_Nasdaq.loc[pd.to_datetime(ROE_Nasdaq.index) == pd.to_datetime(id_dates_1[dat])]).T.dropna()

    aux_roe_nyse.columns = ['ROE']
    aux_roe_nasdaq.columns = ['ROE']

    aux_roe_nyse = aux_roe_nyse.loc[~aux_roe_nyse.index.isin(list(iliquid_assets))] #Elimino aquellas empresa que en ese momento se consideran iliquidas
    aux_roe_nyse = aux_roe_nyse.loc[~aux_roe_nyse.index.isin(Financial_company)]  # Elimino aquellas empresa que en ese momento se consideran financieras

    aux_roe_nasdaq = aux_roe_nasdaq.loc[~aux_roe_nasdaq.index.isin(list(iliquid_assets))] #Elimino aquellas empresa que en ese momento se consideran iliquidas
    aux_roe_nasdaq = aux_roe_nasdaq.loc[~aux_roe_nasdaq.index.isin(Financial_company)]  # Elimino aquellas empresa que en ese momento se consideran financieras

    aux_roe_nyseNasdaq = pd.concat([aux_roe_nyse, aux_roe_nasdaq], axis=0, sort=True)

    per_roe_low_nyse = np.percentile(aux_roe_nyse["ROE"].values, 10) #ROE percentiles
    per_roe_high_nyse = np.percentile(aux_roe_nyse["ROE"].values, 90) #ROE percentiles

    #########MOMENTUM DATE SELECTION (MONTHLY) #######################################
    break_point_month = MV_breakpoint(date=id_dates_1[dat], market_base=MV_NYSE)

    Mom_table = All_month_momentum_returns.loc[((All_month_momentum_returns.index.month == id_dates_1[dat].month) & (All_month_momentum_returns.index.year== id_dates_1[dat].year))].T.dropna()
    Mom_table = Mom_table.join(All_porc_negpos.loc[((All_porc_negpos.index.month == id_dates_1[dat].month) & (All_porc_negpos.index.year == id_dates_1[dat].year))].T.dropna(), how='left', rsuffix='_aux')
    Mom_table = Mom_table.join(All_market_values.loc[All_market_values.index == id_dates_1[dat]].T.dropna(), how='left')
    Mom_table.columns = ['Past11Returns', 'neg-pos', 'Market_value']
    Mom_table["BIG_SMALL_size"] = np.where(Mom_table["Market_value"] >= break_point_month, 1, 0)


    Mom_table = Mom_table.loc[Mom_table["Market_value"] >= perc_40_liquidity_1]

    breakpoints_high_mom = np.percentile(Mom_table["Past11Returns"].values, 70)
    breakpoints_low_mom = np.percentile(Mom_table["Past11Returns"].values, 30)
    Mom_table["SignPastReturns"] = np.where(Mom_table["Past11Returns"] >= breakpoints_high_mom, 1, 0) | np.where(Mom_table["Past11Returns"] <= breakpoints_low_mom, -1, 0)
    Mom_table["FIP"] = Mom_table["SignPastReturns"]*Mom_table["neg-pos"]
    buy_mom_break = np.nanpercentile(Mom_table["FIP"].values, 10)
    sell_mom_break = np.nanpercentile(Mom_table["FIP"].values, 90)

    BUY_BIG_MOM = (Mom_table.loc[((Mom_table["FIP"] <=buy_mom_break) & (Mom_table["BIG_SMALL_size"] == 1))]).dropna()
    BUY_SMALL_MOM = (Mom_table.loc[((Mom_table["FIP"] <= buy_mom_break) & (Mom_table["BIG_SMALL_size"] == 0))]).dropna()
    SELL_SMALL_MOM = (Mom_table.loc[((Mom_table["FIP"] >=sell_mom_break) & (Mom_table["BIG_SMALL_size"] == 0))]).dropna()
    SELL_BIG_MOM = (Mom_table.loc[((Mom_table["FIP"] >= sell_mom_break) & (Mom_table["BIG_SMALL_size"] == 1))]).dropna()

    ####### Rank Low Volatility #################################
    #
    volatility_forecast = {}
    returns_volatility = ((All_prices.loc[All_prices.index <= id_dates_1[dat]]).drop(list(iliquid_assets), axis=1)).resample("BMS").asfreq().pct_change(1)
    returns_volatility.drop(returns_volatility.head(1).index, inplace=True)
    returns_volatility = returns_volatility.dropna(thresh=len(returns_volatility) - 2, axis=1)
    for ass in returns_volatility.columns.values:

        # ass = returns_volatility.columns.values[0]
        try:
            volatility_forecast[ass] = garch.estim_volatility(returns_volatility[ass].dropna(),p=1, o=1, q=1, dist='normal', method='bootstrap', forecast_horiz=1)
        except:
            volatility_forecast[ass] = returns_volatility[ass].std()
    Volatility_table = pd.DataFrame.from_dict(volatility_forecast, orient='index')

    if id_dates_1[dat] in IA_Nasdaq.index: #Si nos encontramos en finales de Junio

        aux_ia_nyse = (IA_NYSE.loc[pd.to_datetime(IA_NYSE.index) == pd.to_datetime(id_dates_1[dat])]).T.dropna()
        aux_ia_nasdaq = (IA_Nasdaq.loc[pd.to_datetime(IA_Nasdaq.index) == pd.to_datetime(id_dates_1[dat])]).T.dropna()

        aux_ia_nyse.columns = ['IA_ratio']
        aux_ia_nasdaq.columns = ['IA_ratio']

        aux_ia_nyse = aux_ia_nyse.loc[~aux_ia_nyse.index.isin(list(iliquid_assets))]  # Elimino aquellas empresa que en ese momento se consideran iliquidas
        aux_ia_nyse = aux_ia_nyse.loc[~aux_ia_nyse.index.isin(Financial_company)]  # Elimino aquellas empresa que en ese momento se consideran financieras

        aux_ia_nasdaq = aux_ia_nasdaq.loc[~aux_ia_nasdaq.index.isin(list(iliquid_assets))]  # Elimino aquellas empresa que en ese momento se consideran iliquidas
        aux_ia_nasdaq = aux_ia_nasdaq.loc[~aux_ia_nasdaq.index.isin(Financial_company)]  # Elimino aquellas empresa que en ese momento se consideran financieras

        aux_ia_nyseNasdaq = pd.concat([aux_ia_nyse, aux_ia_nasdaq], axis=0, sort=True)

        per_ia_low_nyse = np.percentile(aux_ia_nyse["IA_ratio"].values, 10) #ROE percentiles
        per_ia_high_nyse = np.percentile(aux_ia_nyse["IA_ratio"].values, 90) #ROE percentiles

        aux_ia_nyseNasdaq['Clasifc'] = np.where(aux_ia_nyseNasdaq["IA_ratio"] >= per_ia_high_nyse, 1, 0) \
                                       | np.where(aux_ia_nyseNasdaq['IA_ratio'] <= per_ia_low_nyse, 3, 0) \
                                       | np.where(((aux_ia_nyseNasdaq['IA_ratio'] > per_ia_low_nyse) & (aux_ia_nyseNasdaq['IA_ratio'] < per_ia_high_nyse)), 2,0)

        ###################################### LA PARTE DE FAMA FACTORS ############################################################

        median = MV_breakpoint(date=id_dates_1[dat], market_base=MV_NYSE)
        perc_40_liquidity = np.percentile((MV_NYSE.loc[id_dates_1[dat]].dropna().values), 40)

        # We have in Total_assets all NYSE and NASDAQ assets and marked if they are small (0) or big (1)
        NYSE_aux = (MV_NYSE.loc[(MV_NYSE.index.year == id_dates_1[dat].year) &
                                (MV_NYSE.index.month == id_dates_1[dat].month)]).T.dropna()
        Nasdaq_aux = (MV_Nasdaq.loc[(MV_Nasdaq.index.year == id_dates_1[dat].year) &
                                    (MV_Nasdaq.index.month == id_dates_1[dat].month)].T).dropna()

        Total_asset = pd.concat([Nasdaq_aux.rename(columns={id_dates_1[dat]: "Market_value"}),
                                 NYSE_aux.rename(columns={id_dates_1[dat]: "Market_value"})], axis=0, sort=True)
        Total_asset["BIG_SMALL"] = np.where(Total_asset["Market_value"] >= median, 1, 0)
        Total_asset = Total_asset.loc[Total_asset["Market_value"] >= perc_40_liquidity]

        ####### Me traigo la variable SIC_CODE

        Total_ID_company = pd.concat([ID_COMPANY_NYSE, ID_COMPANY_Nasdaq], axis=0, sort=True)
        Total_asset = Total_asset.join(Total_ID_company.set_index("Company_Mnemonic"), how='left')

        Total_asset = Total_asset.loc[((Total_asset["SIC CODE 1"] < 6000) | (Total_asset["SIC CODE 1"] > 6799))]

        aux1 = (MV_NYSE.loc[((MV_NYSE.index.year == id_dates_1[dat].year - 1) & (MV_NYSE.index.month == id_dates_1[dat].month - 6))])

        aux2 = (MV_Nasdaq.loc[((MV_Nasdaq.index.year == id_dates_1[dat].year - 1) & (MV_Nasdaq.index.month == id_dates_1[dat].month - 6))])

        BTMV_NYSE_aux = ((COMMEQUI_NYSE.loc[((COMMEQUI_NYSE.index.year == id_dates_1[dat].year - 1) & (COMMEQUI_NYSE.index.month == id_dates_1[dat].month - 6))] / 1000). \
                         divide(aux1, axis=1)).T.dropna()
        BTMV_NYSE_aux.columns = ["MTBV"]

        BTMV_NASDAQ_aux = ((COMMEQUI_NASDAQ.loc[((COMMEQUI_NASDAQ.index.year == id_dates_1[dat].year - 1) & (COMMEQUI_NASDAQ.index.month == id_dates_1[dat].month - 6))] / 1000). \
                           divide(aux2, axis=1)).T.dropna()
        BTMV_NASDAQ_aux.columns = ["MTBV"]

        # Tengo todos los MTBV de los dos mercados
        BTMV_NASDAQNYSE = pd.concat([BTMV_NYSE_aux, BTMV_NASDAQ_aux], axis=0, sort=True)

        Total_asset = (Total_asset.join(BTMV_NASDAQNYSE, how='left')).sort_values(by=["BIG_SMALL"])

        ############# SMB b/m #######################
        # Obtener los percentiles solo del NYSE que es el breakpoint
        # Assets with BIG capitalization 10-40-10
        big_growth_breakpoint = -np.sort(-(((NYSE_aux.loc[NYSE_aux[id_dates_1[dat]] >= median]).join(BTMV_NYSE_aux,how='left')["MTBV"]).dropna().values))
        big_growth_breakpoint = np.percentile(big_growth_breakpoint[big_growth_breakpoint >= 0], 90)

        big_value_breakpoint = -np.sort(-(((NYSE_aux.loc[NYSE_aux[id_dates_1[dat]] >= median]).join(BTMV_NYSE_aux, how='left')["MTBV"]).dropna().values))
        big_value_breakpoint = np.percentile(big_value_breakpoint[big_value_breakpoint >= 0], 10)

        # Assets with Small capitalization 10-40-10
        small_growth_breakpoint = -np.sort(-(((NYSE_aux.loc[NYSE_aux[id_dates_1[dat]] < median]).join(BTMV_NYSE_aux, how='left')["MTBV"]).dropna().values))
        small_growth_breakpoint = np.percentile(small_growth_breakpoint[small_growth_breakpoint >= 0], 90)

        small_value_breakpoint = -np.sort(-(((NYSE_aux.loc[NYSE_aux[id_dates_1[dat]] < median]).join(BTMV_NYSE_aux, how='left')["MTBV"]).dropna().values))
        small_value_breakpoint = np.percentile(small_value_breakpoint[small_value_breakpoint >= 0], 10)

        Total_asset["SMBbm"] = np.where(((Total_asset["BIG_SMALL"] == 1) & (Total_asset["MTBV"] >= big_growth_breakpoint)), 3, 0) \
                               | np.where(((Total_asset["BIG_SMALL"] == 1) & (Total_asset["MTBV"] <= big_value_breakpoint)), 1, 0) \
                               | np.where(((Total_asset["BIG_SMALL"] == 1) & ((big_value_breakpoint < Total_asset["MTBV"]) & (Total_asset["MTBV"] < big_growth_breakpoint))), 2,0) \
                               | np.where(((Total_asset["BIG_SMALL"] == 0) & (Total_asset["MTBV"] >= small_growth_breakpoint)), 3, 0) \
                               | np.where(((Total_asset["BIG_SMALL"] == 0) & (Total_asset["MTBV"] <= small_value_breakpoint)), 1, 0) \
                               | np.where(((Total_asset["BIG_SMALL"] == 0) & ((small_value_breakpoint < Total_asset["MTBV"]) & (Total_asset["MTBV"] < small_growth_breakpoint))), 2, 0)

        Total_asset = Total_asset.loc[Total_asset["MTBV"] > 0]  #### Me elimino las acciones que tengan un BV negativo

        ############# SMB OP #######################

        # Preparación para concatenar con Total_Assets
        OP_NYSE_aux = (OP_NYSE.loc[(OP_NYSE.index.year == id_dates_1[dat].year) & (OP_NYSE.index.month == id_dates_1[dat].month)]).T.dropna()
        OP_NYSE_aux.columns = ["OP"]

        OP_NASDAQ_aux = (OP_NASDAQ.loc[(OP_NASDAQ.index.year == id_dates_1[dat].year) & (OP_NASDAQ.index.month == id_dates_1[dat].month)]).T.dropna()
        OP_NASDAQ_aux.columns = ["OP"]

        OP_NASDAQNYSE = pd.concat([OP_NYSE_aux, OP_NASDAQ_aux], axis=0, sort=True)

        Total_asset = Total_asset.join(OP_NASDAQNYSE, how='left')

        # Assets with BIG capitalization 10-40-10 OP
        big_robust_breakpoint = -np.sort(-(((NYSE_aux.loc[NYSE_aux[id_dates_1[dat]] >= median]).join(OP_NYSE_aux, how='left')["OP"]).dropna().values))
        big_robust_breakpoint = np.percentile(big_robust_breakpoint, 90)

        big_weak_breakpoint = -np.sort(-(((NYSE_aux.loc[NYSE_aux[id_dates_1[dat]] >= median]).join(OP_NYSE_aux, how='left')["OP"]).dropna().values))
        big_weak_breakpoint = np.percentile(big_weak_breakpoint, 10)

        # Assets with Small capitalization 10-40-10 OP
        small_robust_breakpoint = -np.sort(-(((NYSE_aux.loc[NYSE_aux[id_dates_1[dat]] < median]).join(OP_NYSE_aux, how='left')["OP"]).dropna().values))
        small_robust_breakpoint = np.percentile(small_robust_breakpoint, 90)

        small_weak_breakpoint = -np.sort(-(((NYSE_aux.loc[NYSE_aux[id_dates_1[dat]] < median]).join(OP_NYSE_aux, how='left')["OP"]).dropna().values))
        small_weak_breakpoint = np.percentile(small_weak_breakpoint, 10)

        Total_asset["SMBop"] = np.where(((Total_asset["BIG_SMALL"] == 1) & (Total_asset["OP"] >= big_robust_breakpoint)), 3, 0) \
                               | np.where(((Total_asset["BIG_SMALL"] == 1) & (Total_asset["OP"] <= big_weak_breakpoint)), 1, 0) \
                               | np.where(((Total_asset["BIG_SMALL"] == 1) & ((big_weak_breakpoint < Total_asset["OP"]) & (Total_asset["OP"] < big_robust_breakpoint))), 2, 0) \
                               | np.where(((Total_asset["BIG_SMALL"] == 0) & (Total_asset["OP"] >= small_robust_breakpoint)), 3, 0) \
                               | np.where(((Total_asset["BIG_SMALL"] == 0) & (Total_asset["OP"] <= small_weak_breakpoint)), 1, 0) \
                               | np.where(((Total_asset["BIG_SMALL"] == 0) & ((small_weak_breakpoint < Total_asset["OP"]) & (Total_asset["OP"] < small_robust_breakpoint))), 2, 0)

        Inv_NYSE_aux = Inv_NYSE.loc[Inv_NYSE.index == id_dates_1[dat]].T
        Inv_NYSE_aux.columns = ["INV"]
        Inv_NASDAQ_aux = Inv_NASDAQ.loc[Inv_NASDAQ.index == id_dates_1[dat]].T
        Inv_NASDAQ_aux.columns = ["INV"]

        Inv_NASDAQNYSE = pd.concat([Inv_NYSE_aux, Inv_NASDAQ_aux], axis=0, sort=True)

        Total_asset = Total_asset.join(Inv_NASDAQNYSE, how='left')

        # Assets with BIG capitalization 10-40-10 INV
        big_aggresive_breakpoint = -np.sort(-(((NYSE_aux.loc[NYSE_aux[id_dates_1[dat]] >= median]).join(Inv_NYSE_aux, how='left')["INV"]).dropna().values))
        big_aggresive_breakpoint = np.percentile(big_aggresive_breakpoint, 90)

        big_conserv_breakpoint = -np.sort(-(((NYSE_aux.loc[NYSE_aux[id_dates_1[dat]] >= median]).join(Inv_NYSE_aux, how='left')["INV"]).dropna().values))
        big_conserv_breakpoint = np.percentile(big_conserv_breakpoint, 10)

        # Assets with Small capitalization 10-40-10 INV
        small_aggresive_breakpoint = -np.sort(-(((NYSE_aux.loc[NYSE_aux[id_dates_1[dat]] < median]).join(Inv_NYSE_aux, how='left')["INV"]).dropna().values))
        small_aggresive_breakpoint = np.percentile(small_aggresive_breakpoint, 90)

        small_conserv_breakpoint = -np.sort(-(((NYSE_aux.loc[NYSE_aux[id_dates_1[dat]] < median]).join(Inv_NYSE_aux, how='left')["INV"]).dropna().values))
        small_conserv_breakpoint = np.percentile(small_conserv_breakpoint, 10)

        Total_asset["SMBinv"] = np.where(((Total_asset["BIG_SMALL"] == 1) & (Total_asset["INV"] >= big_aggresive_breakpoint)), 1, 0) \
                                | np.where(((Total_asset["BIG_SMALL"] == 1) & (Total_asset["INV"] <= big_conserv_breakpoint)), 3, 0) \
                                | np.where(((Total_asset["BIG_SMALL"] == 1) & ((big_conserv_breakpoint < Total_asset["INV"]) & (Total_asset["INV"] < big_aggresive_breakpoint))), 2, 0) \
                                | np.where(((Total_asset["BIG_SMALL"] == 0) & (Total_asset["INV"] >= small_aggresive_breakpoint)), 1, 0) \
                                | np.where(((Total_asset["BIG_SMALL"] == 0) & (Total_asset["INV"] <= small_conserv_breakpoint)), 3, 0) \
                                | np.where(((Total_asset["BIG_SMALL"] == 0) & ((small_conserv_breakpoint < Total_asset["INV"]) & (Total_asset["INV"] < small_aggresive_breakpoint))), 2, 0)

        # Separación de las carteras (Portfolios)

        ##############################################################################################################################################

        SMALL_VALUE = Total_asset.loc[(Total_asset["BIG_SMALL"] == 0) & (Total_asset["SMBbm"] == 3)]
        SMALL_NEUTRAL = Total_asset.loc[(Total_asset["BIG_SMALL"] == 0) & (Total_asset["SMBbm"] == 2)]
        SMALL_GROWTH = Total_asset.loc[(Total_asset["BIG_SMALL"] == 0) & (Total_asset["SMBbm"] == 1)]

        BIG_VALUE = Total_asset.loc[(Total_asset["BIG_SMALL"] == 1) & (Total_asset["SMBbm"] == 3)]
        BIG_NEUTRAL = Total_asset.loc[(Total_asset["BIG_SMALL"] == 1) & (Total_asset["SMBbm"] == 2)]
        BIG_GROWTH = Total_asset.loc[(Total_asset["BIG_SMALL"] == 1) & (Total_asset["SMBbm"] == 1)]

        SMALL_ROBUST = Total_asset.loc[(Total_asset["BIG_SMALL"] == 0) & (Total_asset["SMBop"] == 3)]
        SMALL_NEUTRALOP = Total_asset.loc[(Total_asset["BIG_SMALL"] == 0) & (Total_asset["SMBop"] == 2)]
        SMALL_WEAK = Total_asset.loc[(Total_asset["BIG_SMALL"] == 0) & (Total_asset["SMBop"] == 1)]

        BIG_ROBUST = Total_asset.loc[(Total_asset["BIG_SMALL"] == 1) & (Total_asset["SMBop"] == 3)]
        BIG_NEUTRALOP = Total_asset.loc[(Total_asset["BIG_SMALL"] == 1) & (Total_asset["SMBop"] == 2)]
        BIG_WEAK = Total_asset.loc[(Total_asset["BIG_SMALL"] == 1) & (Total_asset["SMBop"] == 1)]

        SMALL_AGGRESIVE = Total_asset.loc[(Total_asset["BIG_SMALL"] == 0) & (Total_asset["SMBinv"] == 3)]
        SMALL_NEUTRALINV = Total_asset.loc[(Total_asset["BIG_SMALL"] == 0) & (Total_asset["SMBinv"] == 2)]
        SMALL_CONSERVATIVE = Total_asset.loc[(Total_asset["BIG_SMALL"] == 0) & (Total_asset["SMBinv"] == 1)]

        BIG_AGGRESIVE = Total_asset.loc[(Total_asset["BIG_SMALL"] == 1) & (Total_asset["SMBinv"] == 3)]
        BIG_NEUTRALINV = Total_asset.loc[(Total_asset["BIG_SMALL"] == 1) & (Total_asset["SMBinv"] == 2)]
        BIG_CONSERVATIVE = Total_asset.loc[(Total_asset["BIG_SMALL"] == 1) & (Total_asset["SMBinv"] == 1)]

    ######### Me construyo los portfolios en cada uno de IA, ROE, SIZE

    aux_roe_nyseNasdaq['Clasifc'] = np.where(aux_roe_nyseNasdaq["ROE"]>=per_roe_high_nyse, 1,0) \
                                    | np.where(aux_roe_nyseNasdaq['ROE']<= per_roe_low_nyse, 3, 0) \
                                    | np.where(((aux_roe_nyseNasdaq['ROE']> per_roe_low_nyse) & (aux_roe_nyseNasdaq['ROE'] < per_roe_high_nyse)), 2, 0)

    aux_size_nyseNasdaq['Clasifc'] = np.where(aux_size_nyseNasdaq["SIZE"] >= per_size_high_nyse, 1, 0) \
                                    | np.where(aux_size_nyseNasdaq['SIZE'] <= per_size_low_nyse, 3, 0) \
                                    | np.where(((aux_size_nyseNasdaq['SIZE'] > per_size_low_nyse) & (aux_size_nyseNasdaq['SIZE'] < per_size_high_nyse)), 2, 0)


    ####### SELECCION DE LAS DISTINTAS CARTERAS

    ######### LOW IA
    LinvLroeSsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 3].index))

    LinvLroeMsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 2].index))

    LinvLroeBsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 1].index))

    LinvMroeSsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 2].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 3].index))

    LinvMroeMsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 2].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 2].index))

    LinvMroeBsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 2].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 1].index))

    LinvHroeSsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 3].index))

    LinvHroeMsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 2].index))

    LinvHroeBsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 1].index))

################ HIGH IA
    HinvLroeSsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 3].index))

    HinvLroeMsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 2].index))

    HinvLroeBsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 1].index))

    HinvMroeSsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 2].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 3].index))

    HinvMroeMsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 2].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 2].index))

    HinvMroeBsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 2].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 1].index))

    HinvHroeSsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 3].index))

    HinvHroeMsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 2].index))

    HinvHroeBsize = find_interes_list(list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 1].index))

    ########## HIGH ROE
    HroeLinvSsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 3].index))

    HroeLinvMsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 2].index))

    HroeLinvBsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 1].index))

    HroeMinvSsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 2].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 3].index))

    HroeMinvMsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 2].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 2].index))

    HroeMinvBsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 2].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 1].index))

    HroeHinvSsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 3].index))

    HroeHinvMsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 2].index))

    HroeHinvBsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 1].index))
    #### LOW ROE
    LroeLinvSsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 3].index))

    LroeLinvMsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 2].index))

    LroeLinvBsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 1].index))

    LroeMinvSsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 2].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 3].index))

    LroeMinvMsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 2].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 2].index))

    LroeMinvBsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 2].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 1].index))

    LroeHinvSsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 3].index))

    LroeHinvMsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 2].index))

    LroeHinvBsize = find_interes_list(list(aux_roe_nyseNasdaq.loc[aux_roe_nyseNasdaq["Clasifc"] == 3].index),
                                      list(aux_ia_nyseNasdaq.loc[aux_ia_nyseNasdaq["Clasifc"] == 1].index),
                                      list(aux_size_nyseNasdaq.loc[aux_size_nyseNasdaq["Clasifc"] == 1].index))

    if id_dates_1[dat] != end_date_variables:
        INV_factor = (calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat+1], LinvLroeSsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat+1], LinvLroeMsize) + \
                     calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat+1], LinvLroeBsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat+1], LinvMroeSsize) + \
                     calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], LinvMroeMsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], LinvMroeBsize) + \
                     calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], LinvHroeSsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], LinvHroeMsize)+ \
                     calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], LinvHroeBsize))/9 - \
                    (calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1],HinvLroeSsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat],id_dates_1[dat + 1], HinvLroeMsize) + \
                    calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], HinvLroeBsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat],id_dates_1[dat + 1], HinvMroeSsize) + \
                    calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], HinvMroeMsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], HinvMroeBsize) + \
                    calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], HinvHroeSsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], HinvHroeMsize) + \
                    calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], HinvHroeBsize)) / 9

        ROE_factor = (calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat+1], HroeLinvSsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat+1], HroeLinvMsize) + \
                     calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat+1], HroeLinvBsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat+1], HroeMinvSsize) + \
                     calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], HroeMinvMsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], HroeMinvBsize) + \
                     calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], HinvHroeSsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], HinvHroeSsize)+ \
                     calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], HroeHinvBsize))/9- \
                     (calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat+1], LroeLinvSsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat+1], LroeLinvMsize) + \
                     calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat+1], LroeLinvBsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat+1], LroeMinvSsize) + \
                     calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], LroeMinvMsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], LroeMinvBsize) + \
                     calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], LinvHroeSsize) + calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], LroeHinvMsize)+ \
                     calc_rend_IAROE(All_prices, All_market_values, id_dates_1[dat], id_dates_1[dat + 1], LroeHinvBsize))/9



        SMBbm = ((((All_prices[list(SMALL_VALUE.index)].loc[((All_prices.index >= id_dates_1[dat]) &
        (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values).\
        dot(SMALL_VALUE["Market_value"].values/ np.sum(SMALL_VALUE["Market_value"].values)) +  (((All_prices[list(SMALL_NEUTRAL.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                      (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values).\
                                                                                                        dot(SMALL_NEUTRAL["Market_value"].values/ np.sum(SMALL_NEUTRAL["Market_value"].values)) + \
                                                                                                   (((All_prices[list(SMALL_GROWTH.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                  (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                    dot(SMALL_GROWTH["Market_value"].values / np.sum(SMALL_GROWTH["Market_value"].values)))/3 - \
        ((((All_prices[list(BIG_VALUE.index)].loc[((All_prices.index >= id_dates_1[dat]) &
         (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
         dot(BIG_VALUE["Market_value"].values / np.sum(BIG_VALUE["Market_value"].values)) + (((All_prices[list(BIG_NEUTRAL.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                    (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                dot(BIG_NEUTRAL["Market_value"].values / np.sum(BIG_NEUTRAL["Market_value"].values)) + \
                                                                                                 (((All_prices[list(BIG_GROWTH.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                    (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                 dot(BIG_GROWTH["Market_value"].values / np.sum(BIG_GROWTH["Market_value"].values))) / 3

        SMBop = ((((All_prices[list(SMALL_ROBUST.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                    (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                 dot(SMALL_ROBUST["Market_value"].values / np.sum(SMALL_ROBUST["Market_value"].values)) + (((All_prices[list(SMALL_NEUTRALOP.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                        (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                        dot(SMALL_NEUTRALOP["Market_value"].values / np.sum(SMALL_NEUTRALOP["Market_value"].values)) + \
                                                                                                        (((All_prices[list(SMALL_WEAK.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                        (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                        dot(SMALL_WEAK["Market_value"].values / np.sum(SMALL_WEAK["Market_value"].values))) / 3 - \
                ((((All_prices[list(BIG_ROBUST.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                   (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(
                    1)).fillna(0).values).dot(BIG_ROBUST["Market_value"].values / np.sum(BIG_ROBUST["Market_value"].values)) + (((All_prices[list(BIG_NEUTRALOP.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                                    (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                                    dot(BIG_NEUTRALOP["Market_value"].values / np.sum(BIG_NEUTRALOP["Market_value"].values)) + \
                                                                                                                    (((All_prices[list(BIG_WEAK.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                                    (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                                    dot(BIG_WEAK["Market_value"].values / np.sum(BIG_WEAK["Market_value"].values))) / 3

        SMBinv = ((((All_prices[list(SMALL_CONSERVATIVE.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                    (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                 dot(SMALL_CONSERVATIVE["Market_value"].values / np.sum(SMALL_CONSERVATIVE["Market_value"].values)) + (((All_prices[list(SMALL_NEUTRALINV.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                        (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                        dot(SMALL_NEUTRALINV["Market_value"].values / np.sum(SMALL_NEUTRALINV["Market_value"].values)) + \
                                                                                                        (((All_prices[list(SMALL_AGGRESIVE.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                        (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                        dot(SMALL_AGGRESIVE["Market_value"].values / np.sum(SMALL_AGGRESIVE["Market_value"].values))) / 3 - \
                ((((All_prices[list(BIG_CONSERVATIVE.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                   (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(
                    1)).fillna(0).values).dot(BIG_CONSERVATIVE["Market_value"].values / np.sum(BIG_CONSERVATIVE["Market_value"].values)) + (((All_prices[list(BIG_NEUTRALINV.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                                    (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                                    dot(BIG_NEUTRALINV["Market_value"].values / np.sum(BIG_NEUTRALINV["Market_value"].values)) + \
                                                                                                                    (((All_prices[list(BIG_AGGRESIVE.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                                    (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                                    dot(BIG_AGGRESIVE["Market_value"].values / np.sum(BIG_AGGRESIVE["Market_value"].values))) / 3
        SMB = (SMBbm+SMBop+SMBinv)/3

        HML = ((((All_prices[list(SMALL_VALUE.index)].loc[((All_prices.index >= id_dates_1[dat]) &
        (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values).\
        dot(SMALL_VALUE["Market_value"].values/ np.sum(SMALL_VALUE["Market_value"].values)) +     (((All_prices[list(BIG_VALUE.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                  (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                    dot(BIG_VALUE["Market_value"].values / np.sum(BIG_VALUE["Market_value"].values)))/2 - \
        ((((All_prices[list(SMALL_GROWTH.index)].loc[((All_prices.index >= id_dates_1[dat]) &
         (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
         dot(SMALL_GROWTH["Market_value"].values / np.sum(SMALL_GROWTH["Market_value"].values)) + (((All_prices[list(BIG_GROWTH.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                    (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                dot(BIG_GROWTH["Market_value"].values / np.sum(BIG_GROWTH["Market_value"].values)))/2


        RMW = ((((All_prices[list(SMALL_ROBUST.index)].loc[((All_prices.index >= id_dates_1[dat]) &
        (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values).\
        dot(SMALL_ROBUST["Market_value"].values/ np.sum(SMALL_ROBUST["Market_value"].values)) +     (((All_prices[list(BIG_ROBUST.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                  (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                    dot(BIG_ROBUST["Market_value"].values / np.sum(BIG_ROBUST["Market_value"].values)))/2 - \
        ((((All_prices[list(SMALL_WEAK.index)].loc[((All_prices.index >= id_dates_1[dat]) &
         (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
         dot(SMALL_WEAK["Market_value"].values / np.sum(SMALL_WEAK["Market_value"].values)) + (((All_prices[list(BIG_WEAK.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                    (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                dot(BIG_WEAK["Market_value"].values / np.sum(BIG_WEAK["Market_value"].values)))/2


        CMA = ((((All_prices[list(SMALL_CONSERVATIVE.index)].loc[((All_prices.index >= id_dates_1[dat]) &
        (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values).\
        dot(SMALL_CONSERVATIVE["Market_value"].values/ np.sum(SMALL_CONSERVATIVE["Market_value"].values)) +  (((All_prices[list(BIG_CONSERVATIVE.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                  (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                    dot(BIG_CONSERVATIVE["Market_value"].values / np.sum(BIG_CONSERVATIVE["Market_value"].values)))/2 - \
        ((((All_prices[list(SMALL_AGGRESIVE.index)].loc[((All_prices.index >= id_dates_1[dat]) &
         (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
         dot(SMALL_AGGRESIVE["Market_value"].values / np.sum(SMALL_AGGRESIVE["Market_value"].values)) + (((All_prices[list(BIG_AGGRESIVE.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                    (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                dot(BIG_AGGRESIVE["Market_value"].values / np.sum(BIG_AGGRESIVE["Market_value"].values)))/2

        weight_month = value_weights_monthly.loc[value_weights_monthly.index==id_dates_1[dat]].fillna(0).values
        assets = value_weights_monthly.loc[value_weights_monthly.index==id_dates_1[dat]].columns.values
        ret_month = (((All_prices[list(assets)].loc[((All_prices.index >= id_dates_1[dat]) &  (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values)
        ret_vec = ret_month.dot(np.transpose(weight_month))

        ################## MOMENTUM FACTOR EXCLUDIONG THE LAST MONTH AND USING THE DEPENDECE OF THE PATH #################################################################

        Momentum_factor = ((((All_prices[list(BUY_BIG_MOM.index)].loc[((All_prices.index >= id_dates_1[dat]) &
        (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values).\
        dot(BUY_BIG_MOM["Market_value"].values/ np.sum(BUY_BIG_MOM["Market_value"].values)) +  (((All_prices[list(BUY_SMALL_MOM.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                  (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                    dot(BUY_SMALL_MOM["Market_value"].values / np.sum(BUY_SMALL_MOM["Market_value"].values)))/2 - \
        ((((All_prices[list(SELL_BIG_MOM.index)].loc[((All_prices.index >= id_dates_1[dat]) &
         (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
         dot(SELL_BIG_MOM["Market_value"].values / np.sum(SELL_BIG_MOM["Market_value"].values)) + (((All_prices[list(SELL_SMALL_MOM.index)].loc[((All_prices.index >= id_dates_1[dat]) &
                                                                                                    (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                                                                                                dot(SELL_SMALL_MOM["Market_value"].values / np.sum(SELL_SMALL_MOM["Market_value"].values)))/2


        Mom_factor_onlybuy = ((((All_prices[list(BUY_BIG_MOM.index)].loc[((All_prices.index >= id_dates_1[dat]) &
        (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values).\
        dot(BUY_BIG_MOM["Market_value"].values/ np.sum(BUY_BIG_MOM["Market_value"].values))) + ((((All_prices[list(BUY_SMALL_MOM.index)].loc[((All_prices.index >= id_dates_1[dat]) &
        (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values).\
        dot(BUY_SMALL_MOM["Market_value"].values/ np.sum(BUY_SMALL_MOM["Market_value"].values)))


        Volatility_factor = ((((All_prices[list(Volatility_table.index)].loc[((All_prices.index >= id_dates_1[dat]) &
        (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values).\
        dot((1/Volatility_table.values)/ np.sum((1/Volatility_table.values))))


        if dat == 0:
            dates_1 = All_prices.loc[((All_prices.index >= id_dates_1[dat]) & (All_prices.index <= id_dates_1[dat+1]))].index[1:]
            FACTORS = pd.DataFrame(np.hstack([INV_factor[1:], ROE_factor[1:], Volatility_factor[1:],ret_vec[1:], np.transpose(np.vstack([SMBbm[1:], SMB[1:], HML[1:], RMW[1:], CMA[1:], Momentum_factor[1:], Mom_factor_onlybuy[1:]]))]),
                                   index=dates_1, columns=['INV', 'ROE', 'LowVolatility' ,'RM', 'SMBbm', 'SMB', 'HML', 'RMW', 'CMA', 'MOMENTUM', 'MOMBUYONLY']) #Volatility_factor[1:]

            Original_factors = pd.DataFrame(np.hstack([INV_factor[1:], ROE_factor[1:], Volatility_factor[1:],ret_vec[1:], np.transpose(np.vstack([SMBbm[1:], SMB[1:], HML[1:], RMW[1:], CMA[1:], Momentum_factor[1:], Mom_factor_onlybuy[1:]]))]),
                                   index=dates_1, columns=['INV', 'ROE', 'LowVolatility' ,'RM', 'SMBbm', 'SMB', 'HML', 'RMW', 'CMA', 'MOMENTUM', 'MOMBUYONLY'])

            ################ Factor volatility Managed ###################################

            if VIX_used: # IF the rollin window is using VIX or just the rolling window of the factors volatility
                volatil_managed = (1/VIX_aux).multiply(((FACTORS.std(axis=0) * np.sqrt(252) * 100) if not static else static_std), axis=1)
                Factors_vol_managed = (pd.DataFrame(index=FACTORS.index)).join(volatil_managed, how='left')

            else:
                Factors_vol_managed = static_std.divide((FACTORS.std(axis=0) * np.sqrt(252) * 100))

            FACTORS = FACTORS.multiply(Factors_vol_managed, axis=1)

            ################ Hago un cluster para escoger los factores que mejor lo han hecho ###########################################

            clust_factors = cluster_corr.cluster_corr_assets(asset_retur=FACTORS.drop(['RM', 'SMB', 'MOMENTUM'], axis=1), asset_names=FACTORS.drop(['RM', 'SMB', 'MOMENTUM'], axis=1).columns.values, plot=False)
            fact_to_use = []
            for key in clust_factors.keys():  # Poner 2 y 1 en los index para coger las acciones. Ahora los he dejado en todos
                fact_to_use.extend(list(((FACTORS[clust_factors[key]]).apply(month_momentum(1), raw=True) *(FACTORS[clust_factors[key]]).apply(momentum_pos_neg(1), raw=True)).sort_values(axis=0).index[:]) if len(clust_factors[key]) > 2 else
                                   list(((FACTORS[clust_factors[key]]).apply(month_momentum(1), raw=True) *(FACTORS[clust_factors[key]]).apply(momentum_pos_neg(1), raw=True)).sort_values(axis=0).index[:]))

            ################ Optimizo con los factores selecionados. Si no se puede ponderaciones iguales a cada factor ####################################################################
            try:
                sub_res = EF_factors_optimization(data_table=FACTORS, point_sep_EF=20, num_sim_MC=1000, factor_to_use=fact_to_use) #"LowVolatility" # I am using the total history adding each month the new set of returns

            except:
                sub_res = pd.DataFrame(np.tile(1/len(fact_to_use), len(fact_to_use)+ 3).reshape((1,len(fact_to_use)+3)), columns= fact_to_use + ["ExpReturn", "StandardDev", "SharpeRatio"], index=[id_dates_1[dat + 1]]) #"LowVolatility"
                print('There is a domain error in the optimization problem')
                errors_date.append(id_dates_1[dat])

            hist_factor_weight = sub_res.rename(index={sub_res.index[0]: id_dates_1[dat + 1]})

        else:
            dates_1 = All_prices.loc[((All_prices.index >= id_dates_1[dat]) & (All_prices.index <= id_dates_1[dat + 1]))].index[1:]

            sub_factors = pd.DataFrame(np.hstack([INV_factor[1:], ROE_factor[1:], Volatility_factor[1:], ret_vec[1:], np.transpose(np.vstack([SMBbm[1:], SMB[1:], HML[1:], RMW[1:], CMA[1:], Momentum_factor[1:], Mom_factor_onlybuy[1:]]))]),
                                                       index=dates_1, columns=['INV', 'ROE', "LowVolatility", 'RM', 'SMBbm', 'SMB', 'HML', 'RMW', 'CMA', 'MOMENTUM', 'MOMBUYONLY']) #Volatility_factor[1:] "LowVolatility",

            Original_factors = pd.concat([Original_factors, pd.DataFrame(np.hstack([INV_factor[1:], ROE_factor[1:], Volatility_factor[1:], ret_vec[1:], np.transpose(np.vstack([SMBbm[1:], SMB[1:], HML[1:], RMW[1:], CMA[1:], Momentum_factor[1:], Mom_factor_onlybuy[1:]]))]),
                                                       index=dates_1, columns=['INV', 'ROE', "LowVolatility", 'RM', 'SMBbm', 'SMB', 'HML', 'RMW', 'CMA', 'MOMENTUM', 'MOMBUYONLY'])], axis=0, sort=True)

            if VIX_used and not Garch_used: # IF the rollin window is using VIX or just the rolling window of the factors volatility
                volatil_managed = (1/VIX_aux).multiply(((sub_factors.std(axis=0) * np.sqrt(252) * 100) if not static else static_std), axis=1)
                Factors_vol_managed = (pd.DataFrame(index=sub_factors.index)).join(volatil_managed, how='left')

            elif Garch_used:
                Factors_vol_managed = pd.DataFrame(index=sub_factors.index)
                try:
                    for factors in Original_factors.columns.values:
                        # factors = Original_factors.columns.values[0]
                        garch11 = arch_model(Original_factors[factors], p=1, o=1, q=1, dist='normal', vol='Garch')
                        res = garch11.fit(update_freq=5)

                        forecasts = res.forecast(horizon=len(sub_factors), method='bootstrap', simulations=1000)
                        sims = forecasts.simulations
                        Factors_vol_managed[factors] = (np.sqrt((np.average(sims.residual_variances[-1, ::10], axis=0))) *np.sqrt(252))*100

                    Factors_vol_managed = (1 / Factors_vol_managed).multiply(((sub_factors.std(axis=0) * np.sqrt(252) * 100) if not static else static_std), axis=1)
                except:

                    volatil_managed = (1 / VIX_aux).multiply(((sub_factors.std(axis=0) * np.sqrt(252) * 100) if not static else static_std), axis=1)
                    Factors_vol_managed = (pd.DataFrame(index=sub_factors.index)).join(volatil_managed, how='left')

            else:
                Factors_vol_managed = static_std.divide((sub_factors.std(axis=0) * np.sqrt(252) * 100))

            sub_factors = sub_factors.multiply(Factors_vol_managed, axis=1)
            FACTORS = pd.concat([FACTORS, sub_factors],axis=0, sort=True)

            clust_factors = cluster_corr.cluster_corr_assets(asset_retur=sub_factors.drop(['RM', 'SMB', 'MOMENTUM'], axis=1), asset_names=sub_factors.drop(['RM', 'SMB', 'MOMENTUM'], axis=1).columns.values, plot=False)
            fact_to_use = []
            for key in clust_factors.keys():  # Poner 2 y 1 en los index para coger las acciones. Ahora los he dejado en todos
                fact_to_use.extend(list(((sub_factors[clust_factors[key]]).apply(month_momentum(1), raw=True) *(sub_factors[clust_factors[key]]).apply(momentum_pos_neg(1), raw=True)).sort_values(axis=0).index[:]) if len(clust_factors[key]) > 2 else
                                   list(((sub_factors[clust_factors[key]]).apply(month_momentum(1), raw=True) *(sub_factors[clust_factors[key]]).apply(momentum_pos_neg(1), raw=True)).sort_values(axis=0).index[:]))

            try:
                sub_res = EF_factors_optimization(data_table=FACTORS, point_sep_EF=20, num_sim_MC=1000, factor_to_use=fact_to_use)  # "LowVolatility" # I am using the total history adding each month the new set of returns

            except:
                sub_res = pd.DataFrame(np.tile(1 / len(fact_to_use), len(fact_to_use) + 3).reshape((1, len(fact_to_use) + 3)), columns=fact_to_use + ["ExpReturn", "StandardDev", "SharpeRatio"], index=[id_dates_1[dat + 1]]) #"LowVolatility"
                print('There is a domain error in the optimization problem')
                errors_date.append(id_dates_1[dat])

            hist_factor_weight = pd.concat([hist_factor_weight, sub_res.rename(index={sub_res.index[0]:id_dates_1[dat+1]})], sort=True)


    if len(hist_factor_weight) > 1:

    ########## RANKING ###############
        # CMArank = pd.DataFrame(((Total_asset.loc[((Total_asset["BIG_SMALL"] == 0) & (Total_asset["SMBinv"] == 1)) | ((Total_asset["BIG_SMALL"] == 1) & (Total_asset["SMBinv"] == 1))]).sort_values(by=['INV'], ascending=False)["INV"]))
        CMArank = pd.DataFrame((Total_asset.loc[((Total_asset["BIG_SMALL"] == 0) | (Total_asset["BIG_SMALL"] == 1)) & ((Total_asset["SMBinv"] == 1) | (Total_asset["SMBinv"] == 3))]).sort_values(by=['INV'])["INV"])
        CMArank["points"] = np.linspace(1, 100, len(CMArank))
        CMArank = CMArank['points']

        # RMWrank = pd.DataFrame((Total_asset.loc[((Total_asset["BIG_SMALL"] == 0) & (Total_asset["SMBop"] == 3)) | ((Total_asset["BIG_SMALL"] == 1) & (Total_asset["SMBop"] == 3))]).sort_values(by=['OP'], ascending=False)["OP"])
        RMWrank =pd.DataFrame((Total_asset.loc[((Total_asset["BIG_SMALL"] == 0) | (Total_asset["BIG_SMALL"] == 1)) & ((Total_asset["SMBop"] == 1) | (Total_asset["SMBop"] == 3))]).sort_values(by=['OP'])["OP"])
        RMWrank["points"] = np.linspace(1, 100, len(RMWrank))
        RMWrank = RMWrank['points']

        # HMLrank =pd.DataFrame((Total_asset.loc[((Total_asset["BIG_SMALL"] == 0) & (Total_asset["SMBbm"] == 3)) | ((Total_asset["BIG_SMALL"] == 1) & (Total_asset["SMBbm"] == 3))]).sort_values(by=['MTBV'], ascending=False)["MTBV"])
        HMLrank =pd.DataFrame((Total_asset.loc[((Total_asset["BIG_SMALL"] == 0) | (Total_asset["BIG_SMALL"] == 1)) & ((Total_asset["SMBbm"] == 1) | (Total_asset["SMBbm"] == 3))]).sort_values(by=['MTBV'])["MTBV"])
        HMLrank["points"] = np.linspace(1, 100, len(HMLrank))
        HMLrank = HMLrank['points']

        # SMBrank =pd.DataFrame(Total_asset.loc[((Total_asset["BIG_SMALL"] == 0) | (Total_asset["BIG_SMALL"] == 1))].sort_values(by=['Market_value'], ascending=True)["Market_value"])
        SMBrank = pd.DataFrame(Total_asset.loc[((Total_asset["BIG_SMALL"] == 0) | (Total_asset["BIG_SMALL"] == 1))].sort_values(by=['Market_value'], ascending=False)["Market_value"])
        SMBrank["points"] = np.linspace(1, 100, len(SMBrank))
        SMBrank = SMBrank['points']

        # MOMrank =pd.DataFrame((pd.concat([BUY_BIG_MOM[["FIP"]], BUY_SMALL_MOM[["FIP"]]], axis=0, sort=True)).sort_values(by=['FIP'], ascending=True)["FIP"])
        MOMrank = pd.DataFrame((pd.concat([BUY_BIG_MOM[["FIP"]], BUY_SMALL_MOM[["FIP"]]], axis=0, sort=True)).sort_values(by=['FIP'], ascending=False)[ "FIP"])
        MOMrank["points"] = np.linspace(1, 100, len(MOMrank))
        MOMrank = MOMrank['points']

        Volatility_table["Vol_rank"] = (1/Volatility_table[Volatility_table.columns.values[0]]) / sum((1/Volatility_table[Volatility_table.columns.values[0]]))
        Volatility_table = Volatility_table.sort_values(by='Vol_rank', ascending=False)
        Volatility_table["points"] = np.linspace(100, 1, len(Volatility_table))
        VolRank = Volatility_table["points"]

        assets_inv_ranking = set(LinvLroeSsize | LinvLroeMsize | LinvLroeBsize | LinvMroeSsize | LinvMroeMsize | LinvMroeBsize | LinvHroeSsize | LinvHroeMsize | LinvHroeBsize |
                                HinvLroeSsize | HinvLroeMsize | HinvLroeBsize | HinvMroeSsize | HinvMroeMsize | HinvMroeBsize | HinvHroeSsize | HinvHroeMsize | HinvHroeBsize)

        assets_roe_ranking = set(HroeLinvSsize | HroeLinvMsize | HroeLinvBsize | HroeMinvSsize | HroeMinvMsize | HroeMinvBsize | HinvHroeSsize | HinvHroeSsize | HroeHinvBsize |
                                LroeLinvSsize | LroeLinvMsize | LroeLinvBsize | LroeMinvSsize | LroeMinvMsize | LroeMinvBsize | LinvHroeSsize | LroeHinvMsize | LroeHinvBsize)


        # aux_ia_size =pd.DataFrame((((pd.concat([aux_ia_nyseNasdaq["IA_ratio"], aux_roe_nyseNasdaq["ROE"], aux_size_nyseNasdaq["SIZE"]], axis=1, sort=True)).T[list(assets_inv_ranking)].T).sort_values(by=['IA_ratio', 'ROE', 'SIZE'], ascending=[False, True, True]))["IA_ratio"])
        aux_ia_size = pd.DataFrame((((pd.concat([aux_ia_nyseNasdaq["IA_ratio"], aux_roe_nyseNasdaq["ROE"], aux_size_nyseNasdaq["SIZE"]], axis=1, sort=True)).T[list(assets_inv_ranking)].T).sort_values(by=['IA_ratio', 'ROE', 'SIZE'], ascending=[False, True, True]))["IA_ratio"])
        aux_ia_size["points"] = np.linspace(1, 100, len(aux_ia_size))
        aux_ia_size = aux_ia_size['points']

        # aux_roe_size = pd.DataFrame((((pd.concat([aux_roe_nyseNasdaq["ROE"], aux_ia_nyseNasdaq["IA_ratio"],  aux_size_nyseNasdaq["SIZE"]], axis=1, sort=True)).T[list(assets_roe_ranking)].T).sort_values(by=['ROE', 'IA_ratio', 'SIZE'], ascending=[False, True, True]))["ROE"])
        aux_roe_size = pd.DataFrame((((pd.concat([aux_roe_nyseNasdaq["ROE"], aux_ia_nyseNasdaq["IA_ratio"], aux_size_nyseNasdaq["SIZE"]], axis=1, sort=True)).T[list(assets_roe_ranking)].T).sort_values(by=['ROE', 'IA_ratio', 'SIZE'], ascending=[True, True, True]))["ROE"])
        aux_roe_size["points"] = np.linspace(1, 100, len(aux_roe_size))
        aux_roe_size = aux_roe_size['points']

        ######### Ranking the assets depending on each weight of EF factors. #############################
        # hist_we_factor_aux = hist_factor_weight.drop(["ExpReturn", "StandardDev", "SharpeRatio"], axis=1) #'LowVolatility'
        # hist_we_factor_aux = hist_we_factor_aux.loc[hist_we_factor_aux.index == id_dates_1[dat]]
        # hist_we_factor_aux.fillna(0, inplace=True)
        # aux_dict = {'CMA': CMArank, 'HML': HMLrank, 'INV':aux_ia_size, 'MOMBUYONLY':MOMrank, 'LowVolatility':VolRank, 'ROE':aux_roe_size, 'SMBbm':SMBrank} # 'RMW':RMWrank
        # fact_to_iter = [aux_dict[x] for x in hist_we_factor_aux.columns.values]

        # contar = 0
        # total_rank = {}
        # for fac in fact_to_iter:
        #     # fac = CMArank.copy()
        #     weight_factor = hist_we_factor_aux.loc[hist_we_factor_aux.index == id_dates_1[dat]].values[0][contar]
        #     for c, value in enumerate(fac.index, 1):
        #         total_rank.setdefault(value, []).append(c * weight_factor / len(fac.index))
        #     contar += 1

        contar = 0
        total_rank = {}
        for fac in [CMArank, RMWrank, HMLrank, SMBrank, MOMrank, aux_ia_size, aux_roe_size]:
            # fac = fact_to_iter[0]
            weight_factor = hist_factor_weight.loc[hist_factor_weight.index == id_dates_1[dat]].values[0][contar]
            for key, value in fac.to_dict().items():
                total_rank.setdefault(key, []).append(value*weight_factor)
            contar += 1

        ranked_assets = pd.DataFrame.from_dict({key: sum(total_rank[key]) for key in total_rank}, orient='index')
        ranked_assets.columns = ["Rank"]
        ranked_assets = ranked_assets.sort_values(by='Rank', ascending=False)
        ranked_assets = ranked_assets.join(All_market_values.loc[All_market_values.index == id_dates_1[dat]].T, how='left')
        ranked_assets.columns = ["Rank", "Market_value"]
        ranked_assets = ranked_assets.iloc[:20]

        ######## MACHINE LEARNING ALGORITHM IN ORDER TO DIVEDE THE ASSETS IN GROUPS OF CORRELATION ######################
        all_assets_returns = ((All_prices[list(ranked_assets.index)].loc[((All_prices.index >= pd.datetime(year=id_dates_1[dat].year-1, month=1, day=1)) & (All_prices.index <= id_dates_1[dat]))]).pct_change(1))
        all_assets_returns = (all_assets_returns.dropna(thresh=len(all_assets_returns) - 2, axis=1)).dropna()
        group_corr_asset = cluster_corr.cluster_corr_assets(asset_retur=all_assets_returns, asset_names=all_assets_returns.columns.values, plot=False)

        assets_to_invest = []
        for key in group_corr_asset.keys(): #Poner 2 y 1 en los index para coger las acciones. Ahora los he dejado en todos
            assets_to_invest.extend(list(ranked_assets.T[group_corr_asset[key]].T["Rank"].sort_values(ascending=False).index[:2]) if len(ranked_assets.T[group_corr_asset[key]].T["Rank"].sort_values(ascending=False)) > 3 else
                                    list(ranked_assets.T[group_corr_asset[key]].T["Rank"].sort_values(ascending=False).index[:1]))

        if id_dates_1[dat] != end_date_variables:

            try:
                ret_assets = ((All_prices[assets_to_invest].loc[((All_prices.index < id_dates_1[dat]) & (All_prices.index >= id_dates_1[dat - 1]))]).pct_change(1)).dropna(axis=0)
                invest_weights = (EF_factors_optimization(data_table=ret_assets, point_sep_EF=20, num_sim_MC=1000, factor_to_use=assets_to_invest)).drop(["ExpReturn", "StandardDev", "SharpeRatio"], axis=1)

                portf_return = ((((All_prices[invest_weights.columns.values].loc[((All_prices.index >= id_dates_1[dat]) & (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                            dot(invest_weights.T.values))[1:]

            except:
                portf_return = ((((All_prices[assets_to_invest].loc[((All_prices.index >= id_dates_1[dat]) & (All_prices.index <= id_dates_1[dat + 1]))]).pct_change(1)).fillna(0).values). \
                            dot(ranked_assets.T[assets_to_invest].T["Market_value"].values / np.sum(ranked_assets.T[assets_to_invest].T["Market_value"].values)))[1:]

            dates_2 = All_prices.loc[((All_prices.index >= id_dates_1[dat]) & (All_prices.index <= id_dates_1[dat + 1]))].index[1:]

            for d in range(0, len(dates_2)):
                Total_portf_returns.setdefault(dates_2[d], []).append(portf_return[d])

        assets_to_invest_conct = [assets_to_invest[x] + (str((id_dates_1[dat]).year)+str((id_dates_1[dat]).month)+ str((id_dates_1[dat]).day)) for x in range(0, len(assets_to_invest))]

        for m in range(0, len(assets_to_invest_conct)):
            Total_summary_investment.setdefault(assets_to_invest_conct[m], []).append((ranked_assets.T[assets_to_invest].T["Market_value"].values / np.sum(
                            ranked_assets.T[assets_to_invest].T["Market_value"].values))[m])

        print(id_dates_1[dat])

################ Recuperar en excel los Total_summary_investment, hist_factor_weight y ranked assets. ##################
# writer = pd.ExcelWriter("C:\\Users\\pccom\\PycharmProjects\\Markowitz\\Portf_with_Vol.xlsx")
# rent_aux2.to_excel(excel_writer=writer, sheet_name='Returns')
# writer.save()

writer = pd.ExcelWriter("C:\\Users\\pccom\\PycharmProjects\\Markowitz\\F.xlsx")
FACTORS.to_excel(excel_writer=writer, sheet_name='Returns')
writer.save()
#

ret_total = {}
for key in Total_portf_returns.keys():

    try:
        ret_total[key] = Total_portf_returns[key][0][0]
    except:
        ret_total[key] = Total_portf_returns[key][0]
#

# FACTORS.std()*np.sqrt(252)
# rent_aux1 = pd.DataFrame.from_dict(Total_portf_returns,orient='index').sort_index()
# rent_aux2 = pd.DataFrame.from_dict(Total_portf_returns,orient='index').sort_index()
# FACTORS["LowVolatility"].std()*np.sqrt(252)
FACTORS[["CMA", "RMW", "HML", "SMBbm", "INV", "ROE", "LowVolatility"]].cumsum().plot()
(pd.DataFrame.from_dict(Total_portf_returns,orient='index').sort_index().cumsum().plot())
(pd.DataFrame.from_dict(ret_total,orient='index').sort_index()).std()*np.sqrt(252)
(pd.DataFrame.from_dict(Total_portf_returns,orient='index').sort_index()).mean()*252

########################################################################################################################
#------------------- Identification of the suspicious assets -----------------------------------------------------------
########################################################################################################################

######## ACCRUAL SCREEN

OL_NYSE = ((((TASSETS_NYSE.subtract(STDEBT_NYSE, axis=1)).subtract(LTD_NYSE, axis=1)).subtract(PREFSTOCK_NYSE, axis=1)).subtract(MININTER_NYSE, axis=1)).subtract(COMMEQUI_NYSE, axis=1)
OA_NYSE = TASSETS_NYSE.subtract(CASHEQUI_NYSE, axis=1)
SNOA_NYSE = OA_NYSE.subtract(OL_NYSE, axis=1) / TASSETS_NYSE

OL_NASDAQ = ((((TASSETS_NASDAQ.subtract(STDEBT_NASDAQ, axis=1)).subtract(LTD_NASDAQ, axis=1)).subtract(PREFSTOCK_NASDAQ, axis=1)).subtract(MININTER_NASDAQ, axis=1)).subtract(COMMEQUI_NASDAQ, axis=1)
OA_NASDAQ = TASSETS_NASDAQ.subtract(CASHEQUI_NASDAQ, axis=1)
SNOA_NASDAQ = OA_NASDAQ.subtract(OL_NASDAQ, axis=1) / TASSETS_NASDAQ

STA_NYSE = NETINCOM_NYSE.subtract(CASHOPER_NYSE, axis=1) / TASSETS_NYSE
STA_NASDAQ = NETINCOM_NASDAQ.subtract(CASHOPER_NASDAQ, axis=1) / TASSETS_NASDAQ



#Variables
RericDay_NASDAQ_annual = (RericDay_NASDAQ.loc[RericDay_NASDAQ.index.month == 7])/ (RericDay_NASDAQ.loc[RericDay_NASDAQ.index.month == 7]).shift(1)
RericDay_NYSE_annual = (RericDay_NYSE.loc[RericDay_NYSE.index.month == 7])/ (RericDay_NYSE.loc[RericDay_NYSE.index.month == 7]).shift(1)

Gross_margin_NASDAQ_annual = (Gross_margin_NASDAQ.loc[Gross_margin_NASDAQ.index.month == 7]).shift(1)/ (Gross_margin_NASDAQ.loc[Gross_margin_NASDAQ.index.month == 7])
Gross_margin_NYSE_annual = (Gross_margin_NYSE.loc[Gross_margin_NYSE.index.month == 7]).shift(1)/ (Gross_margin_NYSE.loc[Gross_margin_NYSE.index.month == 7])

Ass_quality_NASDAQ_annual =((TASSETS_NASDAQ.subtract(CurrAsset_NASDAQ, axis=1)).subtract(PPE_NASDAQ, axis=1))/ TASSETS_NASDAQ
Ass_quality_NASDAQ_annual = Ass_quality_NASDAQ_annual.loc[Ass_quality_NASDAQ_annual.index.month == 7]
Ass_quality_NYSE_annual =((TASSETS_NYSE.subtract(CurrAsset_NYSE, axis=1)).subtract(PPE_NYSE, axis=1))/ TASSETS_NYSE
Ass_quality_NYSE_annual = Ass_quality_NYSE_annual.loc[Ass_quality_NYSE_annual.index.month == 7]

NET_sales_NASDAQ_annual = (NET_sales_NASDAQ.loc[NET_sales_NASDAQ.index.month == 7])/ (NET_sales_NASDAQ.loc[NET_sales_NASDAQ.index.month == 7]).shift(1)
NET_sales_NYSE_annual = (NET_sales_NYSE.loc[NET_sales_NYSE.index.month == 7])/ (NET_sales_NYSE.loc[NET_sales_NYSE.index.month == 7]).shift(1)

Amort_NASDAQ_annual = (Amort_NASDAQ.loc[Amort_NASDAQ.index.month == 7]).shift(1)/ (Amort_NASDAQ.loc[Amort_NASDAQ.index.month == 7])
Amort_NYSE_annual = (Amort_NYSE.loc[Amort_NYSE.index.month == 7]).shift(1)/ (Amort_NYSE.loc[Amort_NYSE.index.month == 7])

SGAI_NASDAQ_annual = GrossProfit_NASDAQ.subtract(OperatingIncome_NASDAQ, axis=1)
SGAI_NASDAQ_annual = SGAI_NASDAQ_annual.loc[SGAI_NASDAQ_annual.index.month == 7] / SGAI_NASDAQ_annual.loc[SGAI_NASDAQ_annual.index.month == 7].shift(1)
SGAI_NYSE_annual = GrossProfit_NYSE.subtract(OperatingIncome_NYSE, axis=1)
SGAI_NYSE_annual = SGAI_NYSE_annual.loc[SGAI_NYSE_annual.index.month == 7] / SGAI_NYSE_annual.loc[SGAI_NYSE_annual.index.month == 7].shift(1)

LVGI_NASDAQ_annual = TotalDebt_NASDAQ.divide(TASSETS_NASDAQ, axis=1)
LVGI_NASDAQ_annual = LVGI_NASDAQ_annual.loc[LVGI_NASDAQ_annual.index.month == 7] / LVGI_NASDAQ_annual.loc[LVGI_NASDAQ_annual.index.month == 7].shift(1)
LVGI_NYSE_annual = TotalDebt_NYSE.divide(TASSETS_NYSE, axis=1)
LVGI_NYSE_annual = LVGI_NYSE_annual.loc[LVGI_NYSE_annual.index.month == 7] / LVGI_NYSE_annual.loc[LVGI_NYSE_annual.index.month == 7].shift(1)

TATA_NYSE = NETINCOMbeforeExtr_NYSE.subtract(CASHOPER_NYSE, axis=1) / TASSETS_NYSE
TATA_NASDAQ = NETINCOMbeforeExtr_NASDAQ.subtract(CASHOPER_NASDAQ, axis=1) / TASSETS_NASDAQ


############## FINANCIAL DISTRESS ##########################

NIMTA_NYSE = NETINCOM_NYSE.divide((TotalLiab_NYSE.add(MV_NYSE, axis=1)), axis=1)
NIMTA_NASDAQ = NETINCOM_NASDAQ.divide((TotalLiab_NASDAQ.add(MV_Nasdaq, axis=1)), axis=1)

TLMTA_NYSE = TotalLiab_NYSE.divide((TotalLiab_NYSE.add(MV_NYSE, axis=1)), axis=1)
TLMTA_NASDAQ = TotalLiab_NASDAQ.divide((TotalLiab_NASDAQ.add(MV_Nasdaq, axis=1)), axis=1)

CASHMTA_NYSE = CASHEQUI_NYSE.divide((TotalLiab_NYSE.add(MV_NYSE, axis=1)), axis=1)
CASHMTA_NASDAQ = CASHEQUI_NASDAQ.divide((TotalLiab_NASDAQ.add(MV_Nasdaq, axis=1)), axis=1)

EXRET_NYSE = (Prices_NYSE.resample("BQS").asfreq()).pct_change(1)
EXRET_NASDAQ = (Prices_Nasdaq.resample("BQS").asfreq()).pct_change(1)

EXRET_SP500 = (Prices_SP500.resample("BQS").asfreq()).pct_change(1)
EXRET_SP500 = EXRET_SP500.loc[((EXRET_SP500.index >= EXRET_NYSE.index[0]) & (EXRET_SP500.index <= EXRET_NYSE.index[-1]))]

EXRET_NYSE = np.log(1+EXRET_NYSE).subtract(np.log(1 + EXRET_SP500["PRICE_INDEX"].values), axis=0)
EXRET_NASDAQ = np.log(1+EXRET_NASDAQ).subtract(np.log(1 + EXRET_SP500["PRICE_INDEX"].values), axis=0)

wts = np.array([0.0666, 0.1333, 0.2666, 0.5333]) # Are the weights given in the book
EXRET_NYSE = EXRET_NYSE.rolling(window=4).apply(f(wts), raw=True)
EXRET_NASDAQ = EXRET_NASDAQ.rolling(window=4).apply(f(wts), raw=True)

MV_SP500 = (MV_SP500.resample("BMS").asfreq())
MV_SP500 = MV_SP500.loc[((MV_SP500.index >= MV_NYSE.index[0]) & (MV_SP500.index <= MV_NYSE.index[-1]))]

NIMTA_NYSE = (NIMTA_NYSE.resample('QS').asfreq()).rolling(window=4).apply(f(wts), raw=True)
NIMTA_NASDAQ = (NIMTA_NASDAQ.resample('QS').asfreq()).rolling(window=4).apply(f(wts), raw=True)

SIGMA_NYSE = (Prices_NYSE.pct_change(1).resample("BQS", label='right').std())*np.sqrt(252)
SIGMA_NASDAQ = (Prices_Nasdaq.pct_change(1).resample("BQS", label='right').std())*np.sqrt(252)

RSIZE_NYSE = np.log(MV_NYSE.iloc[:-2:].divide(MV_SP500["MARKET_VALUE"].values, axis=0))    ######## CAMBIAR CUANDO SE DESCARGUEN LOS DATOS DE SP500
RSIZE_NASDAQ = np.log(MV_Nasdaq.iloc[:-2:].divide(MV_SP500["MARKET_VALUE"].values, axis=0))

MB_NYSE = (TotalLiab_NYSE.add(MV_NYSE*1000, axis=1)).divide(((COMMEQUI_NYSE).add(0.1*(MV_NYSE*1000-COMMEQUI_NYSE), axis=1)), axis=1)
MB_NASDAQ = (TotalLiab_NASDAQ.add(MV_Nasdaq*1000, axis=1)).divide(((COMMEQUI_NASDAQ).add(0.1*(MV_Nasdaq*1000-COMMEQUI_NASDAQ), axis=1)), axis=1)

PRICE_NYSE = Prices_NYSE.where((Prices_NYSE.fillna(0) <= 15), other=15)
PRICE_NYSE = np.log(PRICE_NYSE.resample("BMS").asfreq())
PRICE_NASDAQ = Prices_Nasdaq.where((Prices_Nasdaq.fillna(0) <= 15), other=15)
PRICE_NASDAQ = np.log(PRICE_NASDAQ.resample("BMS").asfreq())


Accrual_screen_nyse = ((STA_NYSE.loc[pd.to_datetime(STA_NYSE.index) == date_elimin_susp_assets]).T)
Accrual_screen_nyse = Accrual_screen_nyse.join(((SNOA_NYSE.loc[pd.to_datetime(SNOA_NYSE.index) == date_elimin_susp_assets]).T), how='left', rsuffix='_r')
Accrual_screen_nyse = Accrual_screen_nyse.rename(columns={Accrual_screen_nyse.columns.values[0]: "STA", Accrual_screen_nyse.columns.values[1]: "SNOA"})

Accrual_screen_nasdaq = ((STA_NASDAQ.loc[pd.to_datetime(STA_NASDAQ.index) == date_elimin_susp_assets]).T)
Accrual_screen_nasdaq = Accrual_screen_nasdaq.join(((SNOA_NASDAQ.loc[pd.to_datetime(SNOA_NASDAQ.index) == date_elimin_susp_assets]).T), how='left', rsuffix='_r')
Accrual_screen_nasdaq = Accrual_screen_nasdaq.rename(columns={Accrual_screen_nasdaq.columns.values[0]: "STA", Accrual_screen_nasdaq.columns.values[1]: "SNOA"})


Accrual_screen = pd.concat([Accrual_screen_nyse, Accrual_screen_nasdaq], axis=0, sort=True)
set_ass = Accrual_screen[["STA", "SNOA"]].T.to_dict(orient='list')
aux_table = {}
for key, value in set_ass.items():
    aux_table[key] = [stats.percentileofscore(Accrual_screen["STA"].dropna().values, value[0]), stats.percentileofscore(Accrual_screen["SNOA"].dropna().values, value[1])]

aux_table = pd.DataFrame.from_dict(aux_table, orient='index')
aux_table.columns = ["Pct_STA", "Pct_SNOA"]
Accrual_screen = Accrual_screen.join(aux_table, how='left')
Accrual_screen["Comboaccrual"] = Accrual_screen[["Pct_STA", "Pct_SNOA"]].mean(axis=1)
Accrual_screen = Accrual_screen.sort_values(by="Comboaccrual", ascending=False, na_position='last')

stock_eliminated = Accrual_screen.loc[Accrual_screen["Comboaccrual"] >= np.nanpercentile(Accrual_screen["Comboaccrual"].values, 95)]


############ Dr. Messod Beneish M-SCORE ##################
Mscore_nyse = ((RericDay_NYSE_annual.loc[pd.to_datetime(RericDay_NYSE_annual.index) == date_elimin_susp_assets]).T)
Mscore_nyse = Mscore_nyse.join(((Gross_margin_NYSE_annual.loc[pd.to_datetime(Gross_margin_NYSE_annual.index) == date_elimin_susp_assets]).T).add_suffix('_r'), how='left')
Mscore_nyse = Mscore_nyse.join(((Ass_quality_NYSE_annual.loc[pd.to_datetime(Ass_quality_NYSE_annual.index) == date_elimin_susp_assets]).T).add_suffix('_r1'), how='left')
Mscore_nyse = Mscore_nyse.join(((NET_sales_NYSE_annual.loc[pd.to_datetime(NET_sales_NYSE_annual.index) == date_elimin_susp_assets]).T).add_suffix('_r2'), how='left')
Mscore_nyse = Mscore_nyse.join(((Amort_NYSE_annual.loc[pd.to_datetime(Amort_NYSE_annual.index) == date_elimin_susp_assets]).T).add_suffix('_r3'), how='left')
Mscore_nyse = Mscore_nyse.join(((SGAI_NYSE_annual.loc[pd.to_datetime(SGAI_NYSE_annual.index) == date_elimin_susp_assets]).T).add_suffix('_r4'), how='left')
Mscore_nyse = Mscore_nyse.join(((LVGI_NYSE_annual.loc[pd.to_datetime(LVGI_NYSE_annual.index) == date_elimin_susp_assets]).T).add_suffix('_r5'), how='left')
Mscore_nyse = Mscore_nyse.join(((TATA_NYSE.loc[pd.to_datetime(TATA_NYSE.index) == date_elimin_susp_assets]).T).add_suffix('_r6'), how='left')
Mscore_nyse = Mscore_nyse.rename(columns={Mscore_nyse.columns.values[0]: "DSRI", Mscore_nyse.columns.values[1]: "GMI", Mscore_nyse.columns.values[2]: "AQI", Mscore_nyse.columns.values[3]: "SGI",
                                          Mscore_nyse.columns.values[4]: "DEPI",Mscore_nyse.columns.values[5]: "SGAI",Mscore_nyse.columns.values[6]: "LVGI",Mscore_nyse.columns.values[7]: "TATA"})

Mscore_NASDAQ = ((RericDay_NASDAQ_annual.loc[pd.to_datetime(RericDay_NASDAQ_annual.index) == date_elimin_susp_assets]).T)
Mscore_NASDAQ = Mscore_NASDAQ.join(((Gross_margin_NASDAQ_annual.loc[pd.to_datetime(Gross_margin_NASDAQ_annual.index) == date_elimin_susp_assets]).T).add_suffix('_r'), how='left')
Mscore_NASDAQ = Mscore_NASDAQ.join(((Ass_quality_NASDAQ_annual.loc[pd.to_datetime(Ass_quality_NASDAQ_annual.index) == date_elimin_susp_assets]).T).add_suffix('_r1'), how='left')
Mscore_NASDAQ = Mscore_NASDAQ.join(((NET_sales_NASDAQ_annual.loc[pd.to_datetime(NET_sales_NASDAQ_annual.index) == date_elimin_susp_assets]).T).add_suffix('_r2'), how='left')
Mscore_NASDAQ = Mscore_NASDAQ.join(((Amort_NASDAQ_annual.loc[pd.to_datetime(Amort_NASDAQ_annual.index) == date_elimin_susp_assets]).T).add_suffix('_r3'), how='left')
Mscore_NASDAQ = Mscore_NASDAQ.join(((SGAI_NASDAQ_annual.loc[pd.to_datetime(SGAI_NASDAQ_annual.index) == date_elimin_susp_assets]).T).add_suffix('_r4'), how='left')
Mscore_NASDAQ = Mscore_NASDAQ.join(((LVGI_NASDAQ_annual.loc[pd.to_datetime(LVGI_NASDAQ_annual.index) == date_elimin_susp_assets]).T).add_suffix('_r5'), how='left')
Mscore_NASDAQ = Mscore_NASDAQ.join(((TATA_NASDAQ.loc[pd.to_datetime(TATA_NASDAQ.index) == date_elimin_susp_assets]).T).add_suffix('_r6'), how='left')
Mscore_NASDAQ = Mscore_NASDAQ.rename(columns={Mscore_NASDAQ.columns.values[0]: "DSRI", Mscore_NASDAQ.columns.values[1]: "GMI", Mscore_NASDAQ.columns.values[2]: "AQI", Mscore_NASDAQ.columns.values[3]: "SGI",
                                          Mscore_NASDAQ.columns.values[4]: "DEPI",Mscore_NASDAQ.columns.values[5]: "SGAI",Mscore_NASDAQ.columns.values[6]: "LVGI",Mscore_NASDAQ.columns.values[7]: "TATA"})

Total_Mscore = pd.concat([Mscore_nyse, Mscore_NASDAQ], axis=0, sort=True) #.fillna(0)).replace(np.inf,0)

Total_Mscore["MScore"] = -4.84 + 0.92*Total_Mscore["DSRI"] + 0.528*Total_Mscore["GMI"] + 0.404*Total_Mscore["AQI"] + 0.892*Total_Mscore["SGI"] + 0.115*Total_Mscore["DEPI"] - 0.172*Total_Mscore["SGAI"] + \
                         4.679*Total_Mscore["TATA"] - 0.327*Total_Mscore["LVGI"]

Total_Mscore["PMAN"] = stats.norm.cdf(Total_Mscore["MScore"], loc=0, scale=1)
Total_Mscore = Total_Mscore.sort_values(by="PMAN", ascending=False)

stock_eliminated_1 = Total_Mscore.loc[Total_Mscore["PMAN"] >= np.nanpercentile(Total_Mscore["PMAN"].loc[Total_Mscore["PMAN"] > 0].values, 95)]


############ IN SEARCH OF DISTRESS RISK CAMPBELL HILSCHER AND SZILAYI ##################
Distress_nyse = ((NIMTA_NYSE.loc[pd.to_datetime(NIMTA_NYSE.index) == date_elimin_susp_assets]).T)
Distress_nyse = Distress_nyse.join(((TLMTA_NYSE.loc[pd.to_datetime(TLMTA_NYSE.index) == date_elimin_susp_assets]).T).add_suffix('_r'), how='left')
Distress_nyse = Distress_nyse.join(((EXRET_NYSE.loc[((pd.to_datetime(EXRET_NYSE.index).month == date_elimin_susp_assets.month) &
                                                     (pd.to_datetime(EXRET_NYSE.index).year == date_elimin_susp_assets.year))]).T).add_suffix('_r1'), how='left')
Distress_nyse = Distress_nyse.join(((SIGMA_NYSE.loc[((pd.to_datetime(SIGMA_NYSE.index).month == date_elimin_susp_assets.month) &
                                                     (pd.to_datetime(SIGMA_NYSE.index).year == date_elimin_susp_assets.year))]).T).add_suffix('_r2'), how='left')
Distress_nyse = Distress_nyse.join(((RSIZE_NYSE.loc[pd.to_datetime(RSIZE_NYSE.index) == date_elimin_susp_assets]).T).add_suffix('_r3'), how='left')
Distress_nyse = Distress_nyse.join(((CASHMTA_NYSE.loc[pd.to_datetime(CASHMTA_NYSE.index) == date_elimin_susp_assets]).T).add_suffix('_r4'), how='left')
Distress_nyse = Distress_nyse.join(((MB_NYSE.loc[pd.to_datetime(MB_NYSE.index) == date_elimin_susp_assets]).T).add_suffix('_r5'), how='left')

Distress_nyse = Distress_nyse.join(((PRICE_NYSE.loc[((pd.to_datetime(PRICE_NYSE.index).month == date_elimin_susp_assets.month) &
                                                     (pd.to_datetime(PRICE_NYSE.index).year == date_elimin_susp_assets.year))]).T).add_suffix('_r6'), how='left')

Distress_nyse = Distress_nyse.rename(columns={Distress_nyse.columns.values[0]: "NIMTAAVG", Distress_nyse.columns.values[1]: "TLMTA", Distress_nyse.columns.values[2]: "EXRETAVG", Distress_nyse.columns.values[3]: "SIGMA",
                                          Distress_nyse.columns.values[4]: "RSIZE",Distress_nyse.columns.values[5]: "CASHMTA",Distress_nyse.columns.values[6]: "MB",Distress_nyse.columns.values[7]: "PRICE"})


Distress_NASDAQ = ((NIMTA_NASDAQ.loc[pd.to_datetime(NIMTA_NASDAQ.index) == date_elimin_susp_assets]).T)
Distress_NASDAQ = Distress_NASDAQ.join(((TLMTA_NASDAQ.loc[pd.to_datetime(TLMTA_NASDAQ.index) == date_elimin_susp_assets]).T).add_suffix('_r'), how='left')
Distress_NASDAQ = Distress_NASDAQ.join(((EXRET_NASDAQ.loc[((pd.to_datetime(EXRET_NASDAQ.index).month == date_elimin_susp_assets.month) &
                                                     (pd.to_datetime(EXRET_NASDAQ.index).year == date_elimin_susp_assets.year))]).T).add_suffix('_r1'), how='left')
Distress_NASDAQ = Distress_NASDAQ.join(((SIGMA_NASDAQ.loc[((pd.to_datetime(SIGMA_NASDAQ.index).month == date_elimin_susp_assets.month) &
                                                     (pd.to_datetime(SIGMA_NASDAQ.index).year == date_elimin_susp_assets.year))]).T).add_suffix('_r2'), how='left')
Distress_NASDAQ = Distress_NASDAQ.join(((RSIZE_NASDAQ.loc[pd.to_datetime(RSIZE_NASDAQ.index) == date_elimin_susp_assets]).T).add_suffix('_r3'), how='left')
Distress_NASDAQ = Distress_NASDAQ.join(((CASHMTA_NASDAQ.loc[pd.to_datetime(CASHMTA_NASDAQ.index) == date_elimin_susp_assets]).T).add_suffix('_r4'), how='left')
Distress_NASDAQ = Distress_NASDAQ.join(((MB_NASDAQ.loc[pd.to_datetime(MB_NASDAQ.index) == date_elimin_susp_assets]).T).add_suffix('_r5'), how='left')

Distress_NASDAQ = Distress_NASDAQ.join(((PRICE_NASDAQ.loc[((pd.to_datetime(PRICE_NASDAQ.index).month == date_elimin_susp_assets.month) &
                                                     (pd.to_datetime(PRICE_NASDAQ.index).year == date_elimin_susp_assets.year))]).T).add_suffix('_r6'), how='left')

Distress_NASDAQ = Distress_NASDAQ.rename(columns={Distress_NASDAQ.columns.values[0]: "NIMTAAVG", Distress_NASDAQ.columns.values[1]: "TLMTA", Distress_NASDAQ.columns.values[2]: "EXRETAVG", Distress_NASDAQ.columns.values[3]: "SIGMA",
                                          Distress_NASDAQ.columns.values[4]: "RSIZE",Distress_NASDAQ.columns.values[5]: "CASHMTA",Distress_NASDAQ.columns.values[6]: "MB",Distress_NASDAQ.columns.values[7]: "PRICE"})


PFD = pd.concat([Distress_nyse, Distress_NASDAQ], axis=0, sort=True) #.fillna(0)).replace(np.inf,0)

PFD["LPFD"] = -9.16 + -20.26*PFD["NIMTAAVG"] + 1.42*PFD["TLMTA"] - 7.13*PFD["EXRETAVG"] + 1.41*PFD["SIGMA"] - 0.045*PFD["RSIZE"] - 2.13*PFD["CASHMTA"] + 0.075*PFD["MB"] - 0.058*PFD["PRICE"]
PFD["PFD"] = 1 / (1 + np.exp(-PFD["LPFD"]))

stock_eliminated_2 = PFD.loc[PFD["PFD"] >= np.nanpercentile(PFD["PFD"].loc[PFD["PFD"] >= 0].values, 95)]



# stock_eliminated.loc[stock_eliminated.index== 'U:PBI']
# stock_eliminated_1.loc[stock_eliminated_1.index== 'U:PBI']
# stock_eliminated_2.loc[stock_eliminated_2.index== 'U:PBI']







