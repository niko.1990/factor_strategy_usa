
def estim_volatility(returns, p, o, q, dist, method, forecast_horiz):
    '''

    :param returns: Series of returns
    :param p: Lag order of the symmetric innovation
    :param o: Lag order of the asymmetric innovationv
    :param q: Lag order of lagged volatility or equivalent
    :param dist: Normal: ‘normal’, ‘gaussian, Students’s t: ‘t’, ‘studentst’, Skewed Student’s t: ‘skewstudent’, ‘skewt’, Generalized Error Distribution: ‘ged’, ‘generalized error”
    :param method: 'bootstrap' or 'simulation'
    :param forecast_horiz: Number of horizon to forecast

    :return: Returns the forecast average volatility for forecast_horiz periods
    '''
    from arch import arch_model
    import numpy as np
    import warnings
    warnings.simplefilter('ignore')

    garch11 = arch_model(returns, p=p, o=o, q=q, dist=dist, vol='Garch')
    res = garch11.fit(update_freq=5)

    forecasts = res.forecast(horizon=forecast_horiz, method=method, simulations=1000)
    sims = forecasts.simulations

    return np.average(np.average(sims.residual_variances[-1, ::10], axis=0))



# lines = plt.plot(sims.residual_variances[-1,::10].T, color='#9cb2d6')
# lines[0].set_label('Simulated path')
# plt.hold('on')
# line = plt.plot(forecasts.variance.iloc[-1].values, color='#002868')
# line[0].set_label('Expected variance')
# legend = plt.legend()

# res.conf_int()
# res.hedgehog_plot(params=None, horizon=10, step=10, start=None, type='volatility', method='bootsptrap', simulations=10)
# res.conditional_volatility.plot()