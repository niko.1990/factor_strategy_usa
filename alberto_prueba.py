import pandas as pd
import numpy as np

def MonteCarlo(rend, nSims):
    '''

    :param rend: Historical daily returns in Data Frame
    :param nSims: number of simulation of the Monte Carlo to employed. Take into account that they have to be at leas 10

    :return: matriz of all simulations
    '''
    import numpy as np
    import scipy.linalg as la
    from scipy import stats

    mat_corr = rend.corr().values
    n, _ = mat_corr.shape
    mat_cholesky = la.cholesky(mat_corr)  # The output of the function is superior triangualr matrix but we want the inferior
    mat_cholesky_inf = la.cholesky(mat_corr).T  # We have the inferior Cholesky matrix
    ys_random_norm = stats.norm.rvs(size=(n, nSims))
    # print("Medias: ", ys_random_norm.mean(axis=0))
    # print("Varianzas: ", ys_random_norm.var(axis=0))
    # print("Correlaciones: ", np.corrcoef(ys_random_norm))

    chol_aux = np.transpose(la.inv(la.cholesky(
        np.cov(ys_random_norm)).T))  # A 1-D or 2-D array containing multiple variables and observations. Each row of x
    # represents a variable, and each column a single observation of all those variables.

    ys_new = np.dot(ys_random_norm.T, chol_aux)  # variables aleatorios modificadas
    ys_correlated = np.dot(ys_new, mat_cholesky_inf.T)  # Cada columna una variable

    stand_dev = np.diag(rend.std(axis=0).values)  # This are the std of the origianl series and I will use it to modify the MC sim
    orig_mus = np.tile((np.mean(rend.values, axis=0, dtype=np.float64)), (ys_correlated.shape[0], 1))

    final_res = np.dot(ys_correlated, stand_dev) + orig_mus

    return final_res


def ResampelEF_func(returns, mus, numSimulation):
    '''
    :param returns: Historical daily returns for the portfolio (Dataframe)
    :param mus: Mean for which to find the optimal portfolio annualized (np.array)
    :param numSimulation: Number of the iterations of the resample

    :return: returns optimal portfolio weights and corresponding sigmas for a desired optimal portfolio return
    '''


    import cvxopt as opt
    from cvxopt import solvers
    import numpy as np
    import pandas as pd

    # Name of all assets
    Index_names = returns.columns.values
    weight_dict = {}
    dict_final = {}
    Ef = {}

    cov = np.matrix(np.cov(returns.T) * 252)  # Matrix covariance original
    _, N = returns.shape

    orig_mus = np.mean(returns, axis=0) * 252  # Original mean of all variables

    # total_mus = np.vstack((orig_mus, random_sample))
    optimal_mus = mus.tolist()
    # constraint matrices for quadratic programming
    P = opt.matrix(cov)
    q = opt.matrix(np.zeros((N, 1)))
    A = opt.matrix(1.0, (1, N))
    b = opt.matrix(1.0)

    # Solve for the origianls mu
    pbar = np.matrix(orig_mus)
    G = opt.matrix(np.concatenate((-np.array(pbar), -np.identity(N)), 0))
    # hide optimization
    opt.solvers.options['show_progress'] = False

    # calculate portfolio weights, every weight vector is of size Nx1
    # find optimal weights with qp(P, q, G, h, A, b)

    optimal_weights = [
        solvers.qp(P, q, G, opt.matrix(np.concatenate((-np.ones((1, 1)) * mu, np.zeros((N, 1))), 0)), A, b)['x'] for mu
        in optimal_mus]

    optimal_sigmas = [np.sqrt(np.matrix(w).T * cov.T.dot(np.matrix(w)))[0, 0] for w in optimal_weights]

    for asset in range(0, len(Index_names)):

        for point in range(0, len(mus)):
            weight_dict.setdefault(Index_names[asset] + "_0", []).append(optimal_weights[point][asset])

    iteration = 1
    # Solve for different simulations
    while iteration != numSimulation:

        simret = MonteCarlo(returns, 10) * 252
        pbar = np.matrix(np.mean(simret, axis=0))
        G = opt.matrix(np.concatenate((-np.array(pbar), -np.identity(N)), 0))
        # hide optimization
        opt.solvers.options['show_progress'] = False

        # calculate portfolio weights, every weight vector is of size Nx1
        # find optimal weights with qp(P, q, G, h, A, b)
        try:
            optimal_weights = [
                solvers.qp(P, q, G, opt.matrix(np.concatenate((-np.ones((1, 1)) * mu, np.zeros((N, 1))), 0)), A, b)['x']
                for mu
                in optimal_mus]

            optimal_sigmas = [np.sqrt(np.matrix(w).T * cov.T.dot(np.matrix(w)))[0, 0] for w in optimal_weights]

            for asset in range(0, len(Index_names)):

                for point in range(0, len(mus)):
                    weight_dict.setdefault(Index_names[asset] + "_%s" % iteration, []).append(
                        optimal_weights[point][asset])

            iteration += 1
        except:
            print("%f" % iteration)

    assets_all = list(set([x.split('_')[:-1][0] for x in list(weight_dict.keys())]))
    for y in assets_all:
        dict_final[y] = np.mean(np.vstack([np.asarray(weight_dict[y + '_%s' % i]) for i in range(iteration)]), axis=0)

    EF_final_resutls = pd.DataFrame.from_dict(dict_final, orient='columns')

    for ef in range(len(mus)):
        media = np.dot(EF_final_resutls.loc[ef:ef].values, (np.mean(returns.values, axis=0, dtype=np.float64)))[0]
        std = np.sqrt(np.dot(np.dot(EF_final_resutls.loc[ef:ef].values, returns.cov().values),
                             EF_final_resutls.loc[:0].values.T))[0][0]
        Ef[ef] = [media, std]

    EF_final_resutls = (EF_final_resutls.join(pd.DataFrame.from_dict(Ef, orient='index'))).rename(
        columns={0: 'ExpReturn', 1: 'StandardDev'})
    EF_final_resutls["SharpeRatio"] = EF_final_resutls['ExpReturn'] / EF_final_resutls['StandardDev']

    opt_portf = EF_final_resutls.loc[EF_final_resutls['SharpeRatio'] == EF_final_resutls['SharpeRatio'].max()]

    return opt_portf, EF_final_resutls


def EF_factors_optimization(data_table, point_sep_EF, num_sim_MC, factor_to_use):
    '''

    :param data_table: DataFrame with all factors
    :param point_sep_EF: point to separate the EF in order to use the modified EF calculation
    :param num_sim_MC: MonteCarlo number of simulation
    :param factor_to_use: Name of the factor to use, should be contained in data_table

    :return: Return the optimal weights of a portfolio
    '''

    import numpy as np
    import scipy.optimize as optim

    optimization_return = data_table[factor_to_use]
    mu = (np.mean(optimization_return.values, axis=0, dtype=np.float64))
    mat_cov = np.cov(optimization_return.values.T)
    xini = np.ndarray((len(mu)))  # Define the intial value iteration

    def portfolio_std(weight):
        '''
        :param weight: numpy vector of weights where each elements is correspondent assets
        :return: The standard deviation of the portfolio
        '''
        import numpy as np
        result = (np.sqrt(np.matmul(np.matmul(weight, mat_cov), weight)))
        return result

    def portfolio_return(mu, weight):
        '''
        :param mu: numpy vector of assets return
        :param weight: numpy vector of weights where each elements is correspondent assets

        :return: Expected returns of the portfolio multiplied by 1000000 and in oposite value
        '''
        import numpy as np

        if mu.shape[0] == weight.shape[0]:

            p_ret = np.dot(mu, weight)
        else:
            p_ret = np.dot(mu, weight.T)

        return -(p_ret) * 1000000

    # Se definen las restricciones
    bounds = [(0, 1) for x in range(0, len(mu))]  # Defino el rango del resultado que sea entre 0 y 1 inclusive

    def con(weight):
        return np.sum(weight) - 1  # si se quieren añadir mas restricciones se añade una lista de diccionarios

    # Resolución el problema
    solMLEopt = optim.minimize(portfolio_return, xini, args=(mu,), method='SLSQP', bounds=bounds,
                               constraints={'type': 'eq', 'fun': con})

    solMLEopt_minstd = optim.minimize(portfolio_std, xini, method='SLSQP', bounds=bounds,
                                      constraints={'type': 'eq', 'fun': con})

    # Maximum return result and its correspondent std
    max_expected_mean = - (portfolio_return(mu, solMLEopt.x)) * 252 / 1000000
    std_dev = np.sqrt(np.matmul(np.matmul(solMLEopt.x, mat_cov), solMLEopt.x)) * np.sqrt(252)

    # Minimum std result and its correspondent expected return
    min_std = portfolio_std(solMLEopt_minstd.x) * np.sqrt(252)
    port_ret = - portfolio_return(mu, solMLEopt_minstd.x) * 252 / 1000000

    increment = (max_expected_mean - port_ret) / (point_sep_EF - 1)

    EF_increment = (np.arange(port_ret, max_expected_mean + increment, increment))[:point_sep_EF]

    optimo, _ = ResampelEF_func(returns=optimization_return, mus=EF_increment, numSimulation=num_sim_MC)

    return optimo


ret = pd.read_csv('C:\\Users\\pccom\\Downloads\\rent_us.csv', sep=',', index_col=0)
activos = pd.read_csv('C:\\Users\\pccom\\Downloads\\activos.csv', sep=',', index_col=0)
ret.drop(ret.head(1).index, inplace=True)


ret = ret.replace(0, np.nan)
ret = ret.dropna(how='any', axis=0)

aux_ret = ret[activos.head(1).values.flatten().tolist()].tail(3000).replace(0, np.nan)
aux_ret.dropna(thresh=2500, inplace=True, axis=1)
aux_ret.dropna(axis=0, inplace=True)

result = (EF_factors_optimization(data_table=aux_ret, point_sep_EF=20, num_sim_MC=1000, factor_to_use=aux_ret.columns.values[:9].tolist())).drop(["ExpReturn", "StandardDev", "SharpeRatio"], axis=1)

