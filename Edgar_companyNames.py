import sqlite3
import requests
import csv
import re
import pandas as pd
import os
import datetime
import re
from bs4 import BeautifulSoup
import numpy as np

# Generate the list of index files archived in EDGAR since start_year (earliest: 1993) until the most recent quarter

# Please download index files chunk by chunk. For example, please first download index files during 1993–2000, then
# download index files during 2001–2005 by changing the following two lines repeatedly, and so on. If you need index
# files up to the most recent year and quarter, comment out the following three lines, remove the comment sign at
# the starting of the next three lines, and define the start_year that immediately follows the ending year of the
# previous chunk.

# start_year = 2011  # change start_year and end_year to re-define the chunk
# current_year = 2015  # change start_year and end_year to re-define the chunk
# current_quarter = 4  # do not change this line

start_year = 2016     # only change this line to download the most recent chunk
current_year = datetime.date.today().year
current_quarter = (datetime.date.today().month - 1) // 3 + 1

years = list(range(start_year, current_year))
quarters = ['QTR1', 'QTR2', 'QTR3', 'QTR4']
history = [(y, q) for y in years for q in quarters]
for i in range(1, current_quarter + 1):
    history.append((current_year, 'QTR%d' % i))
urls = ['https://www.sec.gov/Archives/edgar/full-index/%d/%s/master.idx' % (x[0], x[1]) for x in history]
urls.sort()

# Download index files and write content into SQLite


con = sqlite3.connect("C:\\Users\\pccom\\OneDrive\\HistoricalDataReuters\\SQLitte_DataBase\\EDGAR_INDEX.db")
cur = con.cursor()
aux_type = ''' '{0}-{1}'  '''.format(str(start_year),str(current_year))
cur.execute(''' DROP TABLE IF EXISTS {0} '''.format(aux_type))
cur.execute('CREATE TABLE {0} (cik TEXT, conm TEXT, type TEXT, date TEXT, path TEXT)'.format(aux_type))

for url in urls:
    lines = requests.get(url).content.decode("utf-8", "ignore").splitlines()
    records = [tuple(line.split('|')) for line in lines[11:]]
    cur.executemany(''' INSERT INTO {0} VALUES (?, ?, ?, ?, ?) '''.format(aux_type), records)
    print(url, 'downloaded and wrote to SQLite')

con.commit()
con.close()



####################################### WORKING WITH THE INDEX DOWNLOADED FROM THE SEC-EDGAR DATABASE #########################

######### Select the type 10-Q from the database sql and save the date into csv file.
type_edgar = ''' '10-Q' '''
period = ''' [2006-2010] '''

data_index_edgar = (pd.read_sql_query(''' select * from {1} where type = {0}'''.format(type_edgar, period), con=con))
con.close()
data_index_edgar['cik+date'] = data_index_edgar['cik'].astype(str) + data_index_edgar['date'].astype(str)
data_index_edgar.drop_duplicates('cik+date', keep='last', inplace=True)
data_index_edgar = data_index_edgar.assign(**{'one':((data_index_edgar['path'].astype(str).str.split('/').str[-1]).str.split('.').str[0]).str.split('-').str[0],
                                              'two':((data_index_edgar['path'].astype(str).str.split('/').str[-1]).str.split('.').str[0]).str.split('-').str[1],
                                                'three':((data_index_edgar['path'].astype(str).str.split('/').str[-1]).str.split('.').str[0]).str.split('-').str[2]})

data_index_edgar['newpath'] = (data_index_edgar['path'].astype(str).str.split('/').str[0]) + '/' + \
                              (data_index_edgar['path'].astype(str).str.split('/').str[1]) + '/' + \
                              (data_index_edgar['path'].astype(str).str.split('/').str[2]) + '/' + \
                              data_index_edgar['one'].astype(str) + data_index_edgar['two'].astype(str) + data_index_edgar['three'].astype(str) + '/' + \
                            data_index_edgar['one'].astype(str) + '-' + data_index_edgar['two'].astype(str) + '-' + data_index_edgar['three'].astype(str) + '-' + 'index.htm'

data_index_edgar.to_csv('C:\\Users\\pccom\\PycharmProjects\\Edgar\\10-Q_2006-2010\\index.csv', header=True, index=False)


################ NEW METHOD TO DOWNLOAD THE DATE FROM EDGAR
href_htm_all = {}
with open('C:\\Users\\pccom\\PycharmProjects\\Edgar\\10-Q_2016-2018\\index.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')

    contador = 0
    for line in reader:
        contador += 1
        fn1 = line[0]
        fn2 = re.sub(r'[/\\]', '', line[1])
        fn3 = re.sub(r'[/\\]', '', line[2])
        fn4 = line[3]
        saveas = '-'.join([fn1, fn2, fn3, fn4])
        # Reorganize to rename the output filename.
        url = 'https://www.sec.gov/Archives/' + line[-1].strip()
        r = requests.get('%s' % url)
        soup = BeautifulSoup(r.content)

        aux_all_links = []
        for l in soup.find_all("a"):

            if ('d10q' in ("<a href='%s'>%s</a>" %(l.get("href"), l.text))):
                aux_all_links.append(("<a href='%s'>%s</a>" %(l.get("href"), l.text)))


        href_htm_all[fn1 + fn4] = aux_all_links

        if contador % 1000 == 0:
            print(contador)

data_index_edgar_new = data_index_edgar.set_index('cik+date').join((pd.DataFrame.from_dict(href_htm_all, orient='index'))[0], how='left')
data_index_edgar_new[0].fillna(0, inplace=True)
data_index_edgar_new['New_path'] = np.where(data_index_edgar_new[0] == 0, data_index_edgar_new['path'], data_index_edgar_new[0].astype(str).str.split("'").str[1].str[10:])

(data_index_edgar_new[['cik', 'conm', 'type', 'date', 'New_path']]).to_sql('2016-2018-NEW', con)


# for per in ['1993-2000', '2001-2005', '2006-2010', '2011-2015', '2016-2018']:

    # per = ['1993-2000', '2001-2005', '2006-2010', '2011-2015', '2016-2018'][2]
    # con = sqlite3.connect("C:\\Users\\pccom\\OneDrive\\HistoricalDataReuters\\SQLitte_DataBase\\EDGAR_INDEX.db")
    # ############# Upload the file from sql to folder #####################
    # type_edgar = ''' '10-Q' '''
    # period = ''' [{0}-NEW] '''.format(per)
    #
    # data_index_edgar = (pd.read_sql_query(''' select * from {1} where type = {0}'''.format(type_edgar, period), con=con))
    #
    # con.close()
    # data_index_edgar.drop_duplicates('cik+date', keep='first', inplace=True)
    #
    # data_index_edgar.to_csv('C:\\Users\\pccom\\PycharmProjects\\Edgar\\10-Q_{0}\\index.csv'.format(per), header=True, index=False)

per = '2006-2010'
with open('C:\\Users\\pccom\\PycharmProjects\\Edgar\\10-Q_{0}\\index.csv'.format(per), newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')

    for line in reader:

        fn1 = line[0]
        fn2 = re.sub(r'[/\\]', '', line[1])
        fn3 = re.sub(r'[/\\]', '', line[2])
        fn4 = line[3]
        saveas = '-'.join([fn1, fn2, fn3, fn4])
        # Reorganize to rename the output filename.
        url = 'https://www.sec.gov/Archives/' + line[4].strip()


        with open(saveas, 'wb') as f:
            # r = requests.get('%s' % url)
            # soup = BeautifulSoup(r.content)
            f.write(requests.get('%s' % url).content)
            f.close()
        os.rename(saveas, saveas + ".txt")

        # print(url, 'downloaded and wrote to text file')


        # r = requests.get('%s' % url)
        # soup = BeautifulSoup(r.content)
        #
        # try:
        #     final_results[fn1] = [fn4, soup.prettify()[list((re.search(r'FILENAME', soup.prettify(), re.M | re.I)).span())[-1]:].split()[1]]
        # except:
        #     final_results[fn1] = [fn4, 'NO']

