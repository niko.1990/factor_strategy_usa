import pandas as pd
import sqlite3
from bs4 import BeautifulSoup
import re
import requests
import csv
import random
import numpy as np


con = sqlite3.connect("C:\\Users\\pccom\\OneDrive\\HistoricalDataReuters\\SQLitte_DataBase\\EDGAR_INDEX.db")
type_edgar = ''' '10-Q' '''
period = [''' [1993-2000] ''',  '''[2001-2005]''', '''[2006-2010]''', '''[2011-2015]''', '''[2016-2018]''']
aux_list_cik = []
for p in period:
    # p = period[0]
    aux_list_cik.append((pd.read_sql_query(''' select cik from {1} where type = {0} '''.format(type_edgar, p), con=con))) #and strftime('%Y', {1}.date) = '2010'

Total_CIK = pd.concat(aux_list_cik, axis=0, sort=True)
Total_CIK.drop_duplicates('cik', keep='first', inplace=True)
base_back = '20181231'
con.close()

# ^(?:
#   # mm/dd
#   (1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])
# |
#   # dd/mm
#   (3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])
# )
# # /yyyy
# /[0-9]{4}$

# (?:(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9]) | (3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])) / [0-9]{4}$
#
#
# '(?:(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])|(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9]))/[0-9]{4}$ | (?:(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])|(3[01]|[12][0-9]|0[1-9])-(1[0-2]|0[1-9]))/[0-9]{4}$ | (?:(1[0-2]|0[1-9])(3[01]|[12][0-9]|0[1-9])|(3[01]|[12][0-9]|0[1-9])(1[0-2]|0[1-9]))/[0-9]{4}$'

def replace_month(str):
    import datetime

    a = str.replace('Jan', '01-')
    a = a.replace('Feb', '02_')
    a = a.replace('Mar', '03_')
    a = a.replace('Apr', '04_')
    a = a.replace('May', '05_')
    a = a.replace('Jun', '06_')
    a = a.replace('Jul', '07_')
    a = a.replace('Aug', '08_')
    a = a.replace('Sep', '09_')
    a = a.replace('Oct', '10_')
    a = a.replace('Nov', '11_')
    a = a.replace('Dec', '12_')
    a = a.replace('Q1', '_03_31')
    a = a.replace('Q2', '_06_30')
    a = a.replace('Q3', '_09_30')
    a = a.replace('Q4', '_12_31')

    find = (re.compile('[-+]? (?: (?: \d* \, \d+ ) | (?: \d+ \,? ) )(?: [Ee] [+-]? \d+ ) ? ',re.VERBOSE))

    try:
        isinstance(pd.to_datetime((rx.findall(a)[0]).replace('_','-')), datetime.date)
    except:
        a = find.findall(str)[0] + "_12" + "_31"
    return a


output_path = 'C:\\Users\\pccom\\OneDrive\\HistoricalDataReuters\\SEC_comapny_info'
output_needed = ['From-Period', 'To-Period', 'Present_Date', 'Fiscalyear', 'Currency', 'Type_of_belongs', 'Value']

for cik in Total_CIK.cik.values:
    cik = Total_CIK.cik.values[1] #random.choice(Total_CIK.cik.values)
    # list(Total_CIK.cik.values).index('1401106')

    #Define pattern for dates
    numeric_const_pattern = '\d{4}-\d{2}-\d{2}|\d{4}\d{2}\d{2}|\d{2}_\d{2}_\d{4}|\d{4}_\d{2}_\d{2}|\d{1}_\d{1}_\d{4}|\d{4}_\d{1}_\d{1}|\d{2}_\d{1}_\d{4}|\d{1}_\d{2}_\d{4}'#'(?:[0-9]{2}/){2}[0-9]{2} | (?:[0-9]{2}){2}[0-9]{2}' #'[-+]? (?: (?: \d* \, \d+ ) | (?: \d+ \,? ) )(?: [Ee] [+-]? \d+ ) ? '
    rx = re.compile(numeric_const_pattern, re.VERBOSE)

    path = 'https://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK={0}&type=10-Q&dateb={1}&count=100'.format(cik, base_back)
    r = requests.get('%s' % path)
    soup = BeautifulSoup(r.content, 'html.parser') #< br / >
    tag_list = soup.find_all('a', id='documentsbutton') #< br / >
    href_list = [] #### Here I will get all links reference documentation
    for tag in tag_list: #< br / >
        href_list.append(tag['href'])

    all_year_list = []
    for h in href_list[:]:
        # h = href_list[0]
        auxh = 'https://www.sec.gov/' + h[1:]

        soup1 = BeautifulSoup(requests.get(auxh).content, 'html.parser')
        tag_list_1 = soup1.find_all('table', summary='Data Files')

        if len(tag_list_1) != 0:
            #### Company name search
            soup_sic = BeautifulSoup(requests.get(auxh).text, 'html.parser')
            sic_result = soup_sic.find_all('span', attrs={'class': 'companyName'})
            comp_name = sic_result[0].text.split('\n')[0]

            #### SIC code search
            sic_result = soup_sic.find_all('p', attrs={'class': 'identInfo'})
            sic_code = (sic_result[0].find_all('b'))[0].text
            fiscalyear = (sic_result[0].find_all('strong'))[2].text

            present_period = soup_sic.find_all('div', attrs={'class': 'info'})[-1].text

            result = []
            for th in tag_list_1:
                result.extend(th.find_all('a'))

            listpos = [len(k.text) for k in result if k.text.find('.xml')>0]


            soup2 = BeautifulSoup((requests.get('https://www.sec.gov' + str(result[(listpos.index(min(listpos)))]).split('"')[1])).content, 'html.parser')
            try:
                trading_symbol = soup2.find_all(name=re.compile("(dei:TradingSymbol$)", re.IGNORECASE | re.MULTILINE))[0].text
            except:
                trading_symbol = 'NoMatch'

            # all_variables = {'Num_Share_Out':'dei:EntityCommonStockSharesOutstanding', 'PreferredStock':'us-gaap:TemporaryEquityCarryingAmount', 'Total_Equity':'us-gaap:StockholdersEquityIncludingPortionAttributableToNoncontrollingInterest',
            #                  'CurrentDebt':'us-gaap:DebtCurrent','Liabilities':'us-gaap:Liabilities', 'LongDebt' :'us-gaap:LongTermDebtNoncurrent','Book_value':'us-gaap:CommonStockParOrStatedValuePerShare',
            #                  'Income_before_Extr_items':'us-gaap:IncomeLossFromContinuingOperationsBeforeIncomeTaxesMinorityInterestAndIncomeLossFromEquityMethodInvestments', 'OperatingExpenses': 'us-gaap:OperatingExpenses',
            #                  'Revenues_AssessedTax':'us-gaap:RevenueFromContractWithCustomerExcludingAssessedTax', 'MinorityInterest':'us-gaap:MinorityInterest', 'InterestExpanses':'us-gaap:InterestExpense',
            #                  'CurrentLiabilities':'us-gaap:LiabilitiesCurrent', 'CurrentAssets':'us-gaap:AssetsCurrent', 'CashAndEquivalent':'us-gaap:CashAndCashEquivalentsAtCarryingValue',
            #                  'Amortization_Depr':'us-gaap:DepreciationAndAmortization', 'Cash_Flow_OperActivities':'us-gaap:NetCashProvidedByUsedInOperatingActivities', 'ProfitLoss':'us-gaap:ProfitLoss',
            #                  'TotalAssets':'us-gaap:Assets', 'Revenues':'us-gaap:Revenues', 'TotalShareholdEquity':'us-gaap:StockholdersEquity', 'PPandE':'us-gaap:PropertyPlantAndEquipmentNet',
            #                  'Inventory':'us-gaap:InventoryNet', 'AccountReceivable':'us-gaap:AccountsReceivableNetCurrent', 'OperatingCostsAndExpenses':'us-gaap:OperatingCostsAndExpenses',
            #                  'Cashflow_Investing':'us-gaap:NetCashProvidedByUsedInInvestingActivitiesContinuingOperations','WeightedNumbsharesBasicOutst' :'us-gaap:WeightedAverageNumberOfSharesOutstandingBasic',
            #                  'WeightedNumbsharesDilutedOutst':'us-gaap:WeightedAverageNumberOfDilutedSharesOutstanding',
            #                  'EPS_cont_Oper':'us-gaap:IncomeLossFromContinuingOperationsPerBasicAndDilutedShare', 'Income_continuingOper':'us_gaap:IncomeLossFromContinuingOperations',
            #                  'EPS_basic': 'us-gaap:EarningsPerShareBasic', 'EPS_diluted':'us-gaap:EarningsPerShareDiluted', 'AccountsandNotesReceivable':'us-gaap:AccountsAndNotesReceivableNet',
            #                  'ShortTermBorrowings':'us-gaap:ShortTermBorrowings', 'PreferredStockOutstanging':'us-gaap:PreferredStockSharesOutstanding', 'NetIncomeLoss':'us-gaap:NetIncomeLoss',
            #                  'NetIncomeProfit':'us-gaap:NetIncomeProfit'}

            # soup2.find_all(re.compile("^us-gaap:")) 'If i want to get all us-gaap variables
            all_name_accounts = list(set([o.name for o in soup2.find_all() if (o.name).split(':')[0] == 'us-gaap']))


            list_of_data = []
            for var in all_name_accounts:

                dict_variables = {}

                # var = all_name_accounts[67]
                tags = soup2.find_all(name=re.compile("({0}$)".format(var), re.IGNORECASE | re.MULTILINE))

                for t in tags:
                    try:
                        check = not isinstance(float(t.text), str)
                    except:
                        check = False

                    if check: #+ '/' + rx.findall(t.get('contextref'))[0]

                        dict_variables[var.split(':')[1]] = [pd.to_datetime(((rx.findall(replace_month(t.get('contextref')))[0]).replace('_','-'))) if len(rx.findall(replace_month(t.get('contextref')))) > 1 else 'NoMatch',
                                                             pd.to_datetime(((rx.findall(replace_month(t.get('contextref')))[1]).replace('_', '-'))) if len(rx.findall(replace_month(t.get('contextref')))) >1 else pd.to_datetime(((rx.findall(replace_month(t.get('contextref')))[0]).replace('_','-'))),
                                                             t.get('contextref').split('_')[-1] if len(t.get('contextref').split('_')) > 1 else 'Original',
                                                             t.get('unitref'), comp_name, sic_code, fiscalyear, pd.to_datetime(present_period) , trading_symbol, float(t.text)]
                        aux_fin_table = (pd.DataFrame.from_dict(dict_variables, orient='index'))
                        aux_fin_table.columns = ['From-Period', 'To-Period', 'Type_of_belongs','Currency', 'Company_name', 'SIC_Code', 'Fiscalyear', 'Present_Date', 'Symbol', 'Value']
                        list_of_data.append(aux_fin_table)

            all_year_list.append(pd.concat(list_of_data, axis=0, sort=True))


    if len(all_year_list) > 0:
        Table_to_export = pd.concat(all_year_list, axis=0, sort=True)
        Table_to_export['Duplicates'] = Table_to_export.index.astype(str)+ Table_to_export['From-Period'].astype(str) + Table_to_export['Present_Date'].astype(str) +\
                                        Table_to_export['To-Period'].astype(str) + Table_to_export['Value'].astype(str)
        Table_to_export.drop_duplicates('Duplicates', inplace=True)
        (Table_to_export[output_needed]).to_csv(path_or_buf = output_path + '\\' + list(set(Table_to_export['Company_name'].values))[-1] + '-' + list(set(Table_to_export['SIC_Code'].values))[-1]+ '-' + list(set(Table_to_export['Symbol'].values))[-1] + '.csv', sep=';')
    else:
        print('No InteractiveData')



replace_month(t.get('contextref'))